package com.ehy.order.dto;

import com.ehy.common.model.order.Customer;
import java.util.List;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class OrderDto {

  @NotBlank
  private String userId;
  @NotNull
  private InventoryDetail inventory;
  @NotEmpty @Size(min = 1)
  private List<Customer> customers;
}

package com.ehy.order.dto;

import com.ehy.common.model.BaseKafkaSource;
import com.ehy.common.model.kafka.DocumentKey;
import com.ehy.order.dto.domain.Order;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Data
@NoArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
@EqualsAndHashCode(callSuper = true)
@ToString(callSuper = true)
public class OrderEvent extends BaseKafkaSource<Order> {

  public OrderEvent(String operationType, Order fullDocument,
      DocumentKey documentKey) {
    super(operationType, fullDocument, documentKey);
  }
}

package com.ehy.order.dto;

import java.util.Set;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotBlank.List;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class InventoryDetail {

  @NotEmpty
  @Size(min = 1, max = 2)
  @List(@NotBlank)
  private Set<String> inventoryIds;
  @NotNull
  @Min(1)
  private Integer quantity;
}

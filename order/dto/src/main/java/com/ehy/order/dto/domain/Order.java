package com.ehy.order.dto.domain;

import com.ehy.common.enums.OrderStatus;
import com.ehy.common.model.order.OrderDetail;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import java.util.List;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Data
@NoArgsConstructor
@EqualsAndHashCode
@ToString(callSuper = true)
@JsonIgnoreProperties(ignoreUnknown = true)
public class Order{

  private String id;
  private OrderDetail orderDetail;
  private OrderStatus status;
  private List<CancellationPolicy> cancellationPolicies;
  private Double refundAmount;
  private String updatedBy;
}

package com.ehy.order.dto;

import com.ehy.common.model.BaseDto;
import javax.validation.constraints.NotBlank;
import lombok.Builder;
import lombok.Data;

@Data
public class SystemParameterDto extends BaseDto<String> {

  @NotBlank
  private String variable;
  @NotBlank
  private String value;
  @NotBlank
  private String description;

  public SystemParameterDto() {
  }

  @Builder
  public SystemParameterDto(String id,
      @NotBlank String variable, @NotBlank String value,
      @NotBlank String description) {
    super(id);
    this.variable = variable;
    this.value = value;
    this.description = description;
  }
}

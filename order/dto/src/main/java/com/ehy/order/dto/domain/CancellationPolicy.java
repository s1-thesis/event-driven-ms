package com.ehy.order.dto.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@JsonIgnoreProperties(ignoreUnknown = true)
public class CancellationPolicy {

  private String inventoryId;
  private boolean refundable;
  private Integer daysBefore;
  private Double percentage;
}

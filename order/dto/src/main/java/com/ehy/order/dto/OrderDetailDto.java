package com.ehy.order.dto;

import com.ehy.common.enums.OrderStatus;
import com.ehy.common.model.order.OrderDetail;
import com.ehy.order.dto.domain.CancellationPolicy;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Builder
@Data
@AllArgsConstructor
@NoArgsConstructor
public class OrderDetailDto {

    private OrderDetail orderDetail;
    private OrderStatus status;
    private List<CancellationPolicy> cancellationPolicies;
    private Double refundAmount;
}

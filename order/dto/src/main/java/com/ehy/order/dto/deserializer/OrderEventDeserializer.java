package com.ehy.order.dto.deserializer;

import com.ehy.order.dto.OrderEvent;
import io.quarkus.kafka.client.serialization.ObjectMapperDeserializer;

public class OrderEventDeserializer extends ObjectMapperDeserializer<OrderEvent> {

  public OrderEventDeserializer() {
    super(OrderEvent.class);
  }
}

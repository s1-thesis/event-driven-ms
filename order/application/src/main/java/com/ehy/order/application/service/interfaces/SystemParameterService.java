package com.ehy.order.application.service.interfaces;

import com.ehy.order.application.entity.domain.SystemParameter;

public interface SystemParameterService extends BaseResourceService<SystemParameter> {

}

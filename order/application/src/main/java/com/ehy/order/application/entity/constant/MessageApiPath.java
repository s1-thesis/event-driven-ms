package com.ehy.order.application.entity.constant;

public class MessageApiPath {

  public static final String BASE_PATH = "/message-service";
  public static final String EMAIL = "/email";
  public static final String SEND = "/send";
}

package com.ehy.order.application.entity.constant;

public class ApiPath {

  /**
   * Rename this with service name
   */
  public static final String BASE_PATH = "/order-service";

  public static final String APPEND_SYSTEM_PARAMETERS = "/system-parameters";
  public static final String APPEND_ID = "/{id}";
  public static final String APPEND_ORDER = "/orders";
  public static final String APPEND_CONFIRM = "/confirm";
  public static final String APPEND_REFUND = "/refund";
  public static final String APPEND_VALIDATE_REFUND = "/refund/validate";
  public static final String APPEND_CANCEL = "/cancel";
  public static final String APPEND_REJECT = "/reject";

  /**
   * Base path
   */
  public static final String BASE_PATH_SYSTEM_PARAMETERS = BASE_PATH + APPEND_SYSTEM_PARAMETERS;
  public static final String BASE_PATH_ORDER = BASE_PATH + APPEND_ORDER;
}

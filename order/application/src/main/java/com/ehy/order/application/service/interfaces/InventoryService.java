package com.ehy.order.application.service.interfaces;

import com.ehy.order.application.entity.domain.Inventory;
import com.ehy.order.application.entity.enums.Action;
import com.ehy.product.dto.InventoryDto;
import io.smallrye.mutiny.Uni;

public interface InventoryService extends BaseResourceService<Inventory> {

  Uni<Void> upsert(InventoryDto inventoryDto, Action action);
}

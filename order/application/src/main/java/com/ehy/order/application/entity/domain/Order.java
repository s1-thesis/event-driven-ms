package com.ehy.order.application.entity.domain;

import com.ehy.common.enums.OrderStatus;
import com.ehy.common.model.order.OrderDetail;
import com.ehy.order.application.entity.constant.CollectionName;
import io.quarkus.mongodb.panache.MongoEntity;
import java.util.List;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Data
@NoArgsConstructor
@EqualsAndHashCode(callSuper = true)
@ToString(callSuper = true)
@MongoEntity(collection = CollectionName.ORDER)
public class Order extends BaseMongo {

  private OrderDetail orderDetail;
  private OrderStatus status;
  private List<CancellationPolicy> cancellationPolicies;
  private Double refundAmount;
}

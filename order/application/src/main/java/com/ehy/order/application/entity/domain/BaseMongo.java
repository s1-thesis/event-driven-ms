package com.ehy.order.application.entity.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import io.quarkus.mongodb.panache.reactive.ReactivePanacheMongoEntityBase;
import java.time.Instant;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.bson.codecs.pojo.annotations.BsonId;
import org.bson.types.ObjectId;

@Data
@EqualsAndHashCode(callSuper = true)
@JsonIgnoreProperties(ignoreUnknown = true)
public abstract class BaseMongo extends ReactivePanacheMongoEntityBase {
  @BsonId
  public ObjectId id;
  private String createdBy;
  private String updatedBy;
  private Instant createdAt;
  private Instant updatedAt;
  private boolean deleted;

  public BaseMongo() {
  }

  public BaseMongo(ObjectId id, String createdBy, String updatedBy, Instant createdAt,
      Instant updatedAt, boolean deleted) {
    this.id = id;
    this.createdBy = createdBy;
    this.updatedBy = updatedBy;
    this.createdAt = createdAt;
    this.updatedAt = updatedAt;
    this.deleted = deleted;
  }
}

package com.ehy.order.application.libraries.mapper;

import com.ehy.common.model.order.InventoryDetail;
import com.ehy.order.application.entity.domain.Order;
import com.ehy.order.dto.OrderDto;
import java.util.ArrayList;
import java.util.List;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;
import org.mapstruct.Named;

@Mapper(componentModel = "cdi")
public interface OrderMapper extends BaseMapper {

  @Mappings({
      @Mapping(source = "dto.userId", target = "orderDetail.userId"),
      @Mapping(source = "dto.customers", target = "orderDetail.customers"),
      @Mapping(source = "dto.inventory", target = "orderDetail.inventoryDetails", qualifiedByName = "mapInventory"),
  })
  Order toDomain(OrderDto dto);

  @Named("mapInventory")
  default List<InventoryDetail> mapInventory(com.ehy.order.dto.InventoryDetail inventoryDto) {
    List<InventoryDetail> res = new ArrayList<>();
    inventoryDto.getInventoryIds().forEach(id -> res
        .add(InventoryDetail.builder().id(id).quantity(inventoryDto.getQuantity()).build()));

    return res;
  }
}

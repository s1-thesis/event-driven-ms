package com.ehy.order.application.listener.kafka;

import com.ehy.common.enums.OrderStatus;
import com.ehy.common.enums.PaymentStatus;
import com.ehy.common.model.MandatoryRequest;
import com.ehy.order.application.service.interfaces.OrderService;
import com.ehy.payment.dto.PaymentEvent;
import com.ehy.payment.dto.TransactionDto;
import java.util.concurrent.CompletionStage;
import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import lombok.extern.slf4j.Slf4j;
import org.eclipse.microprofile.reactive.messaging.Incoming;

@ApplicationScoped
@Slf4j
public class PaymentListener {

  @Inject
  OrderService orderService;

  @Incoming("payment")
  public CompletionStage<Void> paymentListener(PaymentEvent paymentEvent) {
    log.info("[KAFKA] Receive payment event = {}", paymentEvent);

    TransactionDto transaction = paymentEvent.getFullDocument();
    OrderStatus orderStatus;

    if (PaymentStatus.CONFIRMED.equals(transaction.getStatus())) {
      orderStatus = OrderStatus.CONFIRMED;
    } else if (PaymentStatus.DENIED.equals(transaction.getStatus())) {
      orderStatus = OrderStatus.FAILED;
    } else if (PaymentStatus.REFUNDED.equals(transaction.getStatus())) {
      orderStatus = OrderStatus.REFUNDED;
    } else {
      orderStatus = OrderStatus.PROCESS;
    }

    return orderService
        .changeStatus(MandatoryRequest.builder().username(transaction.getUpdatedBy()).build(),
            transaction.getOrderId(), orderStatus)
        .onFailure().invoke(t -> log
            .error("Error processing payment event, order id = {}", transaction.getOrderId(), t))
        .subscribeAsCompletionStage()
        .thenAccept(t -> log.info("Successfully receiving payment for order id = {}",
            transaction.getOrderId()));
  }
}

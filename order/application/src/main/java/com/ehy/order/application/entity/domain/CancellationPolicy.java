package com.ehy.order.application.entity.domain;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class CancellationPolicy {

  private String inventoryId;
  private boolean refundable;
  private Integer daysBefore;
  private Double percentage;
}

package com.ehy.order.application.web.controller;

import com.ehy.common.model.BaseResponse;
import com.ehy.common.model.MandatoryRequest;
import com.ehy.order.application.entity.constant.ApiPath;
import com.ehy.order.application.libraries.mapper.OrderMapper;
import com.ehy.order.application.service.interfaces.OrderService;
import com.ehy.order.dto.OrderDetailDto;
import com.ehy.order.dto.OrderDto;
import io.smallrye.mutiny.Uni;
import org.eclipse.microprofile.openapi.annotations.tags.Tag;
import org.eclipse.microprofile.openapi.annotations.tags.Tags;
import org.jboss.resteasy.annotations.jaxrs.PathParam;

import javax.inject.Inject;
import javax.validation.Valid;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;

@Path(ApiPath.BASE_PATH_ORDER)
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
@Tags(value = @Tag(name = "Order", description = "Order API"))
public class OrderController extends BaseResource {

  @Inject
  OrderService orderService;

  @Inject
  OrderMapper orderMapper;

  @GET
  @Path(ApiPath.APPEND_ID)
  public Uni<BaseResponse<OrderDetailDto>> getDetail(
          @BeanParam @Valid MandatoryRequest mandatoryRequest,
          @PathParam String id) {
    return buildResponse(orderService.getOrderDetail(mandatoryRequest, id));
  }

  @POST
  public Uni<BaseResponse<String>> book(
      @BeanParam @Valid MandatoryRequest mandatoryRequest,
      @Valid OrderDto orderDto) {
    return buildResponse(orderService.createOrder(mandatoryRequest, orderMapper.toDomain(orderDto)));
  }

  @POST
  @Path(ApiPath.APPEND_ID + ApiPath.APPEND_VALIDATE_REFUND)
  public Uni<BaseResponse<Boolean>> validateRefund(
      @BeanParam @Valid MandatoryRequest mandatoryRequest,
      @PathParam String id) {
    return buildResponse(orderService.validateRefund(mandatoryRequest, id));
  }

  @POST
  @Path(ApiPath.APPEND_ID + ApiPath.APPEND_CANCEL)
  public Uni<BaseResponse<Boolean>> cancel(
      @BeanParam @Valid MandatoryRequest mandatoryRequest,
      @PathParam String id) {
    return buildResponse(orderService.cancel(mandatoryRequest, id));
  }
}

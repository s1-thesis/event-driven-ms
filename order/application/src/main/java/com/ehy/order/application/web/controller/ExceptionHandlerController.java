package com.ehy.order.application.web.controller;

import com.ehy.common.constant.ResponseCode;
import com.ehy.common.exception.BusinessLogicException;
import com.ehy.common.helper.BaseResponseHelper;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;
import lombok.extern.slf4j.Slf4j;
import org.hibernate.validator.internal.engine.path.PathImpl;

@Slf4j
public class ExceptionHandlerController {

  public ExceptionHandlerController() {
  }

  @Provider
  public static class BusinessLogicExceptionHandler implements
      ExceptionMapper<BusinessLogicException> {

    public BusinessLogicExceptionHandler() {
    }

    @Override
    public Response toResponse(BusinessLogicException e) {
      log.error("BusinessLogicException thrown", e);
      return Response.status(Status.OK)
          .entity(BaseResponseHelper.build(e.getCode(), e.getMessage(), null, e.getErrors()))
          .build();
    }
  }

  @Provider
  public static class ExceptionHandler implements ExceptionMapper<Exception> {

    public ExceptionHandler() {
    }

    @Override
    public Response toResponse(Exception e) {
      log.error("Exception thrown", e);
      return Response.status(Status.INTERNAL_SERVER_ERROR)
          .entity(BaseResponseHelper.build(ResponseCode.SYSTEM_ERROR.getCode(),
              ResponseCode.SYSTEM_ERROR.getMessage(), null,
              Collections.singletonList(e.getMessage())))
          .build();
    }
  }

  @Provider
  public static class ConstraintViolationExceptionHandler implements
      ExceptionMapper<ConstraintViolationException> {

    public ConstraintViolationExceptionHandler() {
    }

    @Override
    public Response toResponse(ConstraintViolationException e) {
      log.error("ConstraintViolationException thrown", e);
      List<ConstraintViolation> errors = new ArrayList<>(e.getConstraintViolations());
      List<String> errorString = new ArrayList<>();
      errors.forEach(ex -> {
        String path = ((PathImpl) ex.getPropertyPath()).getLeafNode().getName();

        errorString.add(path + " " + ex.getMessage());
      });

      return Response.status(Status.OK)
          .entity(BaseResponseHelper.build(ResponseCode.BIND_ERROR.getCode(),
              ResponseCode.BIND_ERROR.getMessage(), null, errorString))
          .build();
    }
  }
}

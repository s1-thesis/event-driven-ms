package com.ehy.order.application.entity.constant;

public class PaymentApiPath {

  public static final String BASE_PATH = "/payment-service";
  public static final String APPEND_TRANSACTION = "/transactions";
}

package com.ehy.order.application.service.impl;

import com.ehy.common.model.MandatoryRequest;
import com.ehy.order.application.dao.interfaces.BaseRepository;
import com.ehy.order.application.dao.interfaces.InventoryRepository;
import com.ehy.order.application.entity.domain.Inventory;
import com.ehy.order.application.entity.enums.Action;
import com.ehy.order.application.libraries.mapper.InventoryMapper;
import com.ehy.order.application.service.interfaces.InventoryService;
import com.ehy.product.dto.InventoryDto;
import io.smallrye.mutiny.Uni;
import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import lombok.extern.slf4j.Slf4j;

@ApplicationScoped
@Slf4j
public class InventoryServiceImpl extends BaseResourceServiceImpl<Inventory> implements InventoryService {

  @Inject
  InventoryRepository inventoryRepository;

  @Inject
  InventoryMapper inventoryMapper;

  @Override
  protected BaseRepository<Inventory> repository() {
    return inventoryRepository;
  }

  @Override
  protected Inventory mapUpdate(Inventory oldData, Inventory newData) {
    return null;
  }

  @Override
  public Uni<Void> upsert(InventoryDto inventoryDto, Action action) {
    return Uni.createFrom().deferred(() -> repository()
        .upsert(MandatoryRequest.builder().username(inventoryDto.getUpdatedBy()).build(),
            inventoryMapper.toDomain(inventoryDto), Action.CREATE));
  }
}

package com.ehy.order.application.entity.constant;

public class InventoryApiPath {

  public static final String BASE_PATH = "/product-service";
  public static final String INVENTORIES = "/inventories";
  public static final String ALLOTMENT = "/allotment";
}

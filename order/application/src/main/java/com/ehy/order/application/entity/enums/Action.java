package com.ehy.order.application.entity.enums;

public enum Action {
  CREATE, UPDATE, DELETE
}

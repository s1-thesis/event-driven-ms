package com.ehy.order.application.dao.interfaces;

import com.ehy.order.application.entity.domain.Inventory;

public interface InventoryRepository extends BaseRepository<Inventory> {

}

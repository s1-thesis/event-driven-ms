package com.ehy.order.application.dao.impl;

import com.ehy.order.application.dao.interfaces.SystemParameterRepository;
import com.ehy.order.application.entity.domain.SystemParameter;
import javax.enterprise.context.ApplicationScoped;

@ApplicationScoped
public class SystemParameterRepositoryImpl extends BaseRepositoryImpl<SystemParameter> implements SystemParameterRepository {

}

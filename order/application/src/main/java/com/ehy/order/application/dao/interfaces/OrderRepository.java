package com.ehy.order.application.dao.interfaces;

import com.ehy.order.application.entity.domain.Order;

public interface OrderRepository extends BaseRepository<Order> {

}

package com.ehy.order.application.dao.impl;

import com.ehy.common.model.MandatoryRequest;
import com.ehy.order.application.dao.interfaces.BaseRepository;
import com.ehy.order.application.entity.constant.BaseMongoFields;
import com.ehy.order.application.entity.domain.BaseMongo;
import com.ehy.order.application.entity.enums.Action;
import com.ehy.order.application.libraries.helper.CommonHelper;
import io.quarkus.mongodb.panache.reactive.ReactivePanacheQuery;
import io.quarkus.panache.common.Page;
import io.quarkus.panache.common.Sort;
import io.smallrye.mutiny.Multi;
import io.smallrye.mutiny.Uni;
import java.util.List;
import org.bson.types.ObjectId;

public class BaseRepositoryImpl<T extends BaseMongo> implements BaseRepository<T> {

  @Override
  public Multi<T> findAllByDeleted(boolean deleted) {
    return this.find(BaseMongoFields.DELETED + " = ?1", deleted, Sort.ascending(BaseMongoFields.ID))
        .stream();
  }

  @Override
  public ReactivePanacheQuery<T> findAllByDeleted(boolean deleted, Page page) {
    return this.find(BaseMongoFields.DELETED + " = ?1", deleted, Sort.ascending(BaseMongoFields.ID))
        .page(page);
  }

  @Override
  public Uni<T> findByIdAndDeleted(String id, boolean deleted) {
    return this.find(BaseMongoFields.ID + " = ?1 and " + BaseMongoFields.DELETED + " = ?2",
        new ObjectId(id), deleted).firstResult();
  }

  @Override
  public Uni<Long> countByDeleted(boolean deleted) {
    return this.count(BaseMongoFields.DELETED + " = ?1", deleted);
  }

  @Override
  public Uni<Void> upsert(MandatoryRequest mandatoryRequest, T t, Action action) {
    return this.persistOrUpdate(CommonHelper.setBaseMongoFields(mandatoryRequest, t, action));
  }

  /**
   * BARBAR
   */
  @Override
  public Uni<List<T>> findAllByIdsAndDeleted(List<String> ids, boolean deleted) {
    StringBuilder idQuery = new StringBuilder();

    for (int i = 0; i < ids.size(); i++) {
      idQuery.append("ObjectId(\"").append(ids.get(i)).append("\")");
      if (i != ids.size() - 1) {
        idQuery.append(",");
      }
    }

    return this
        .list("{" + BaseMongoFields.ID + ": {$in: [ " + idQuery + "]}, " + BaseMongoFields.DELETED
                + ": ?1" + "}",
            deleted);
  }
}

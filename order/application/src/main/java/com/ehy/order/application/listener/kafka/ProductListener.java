package com.ehy.order.application.listener.kafka;

import com.ehy.order.application.entity.enums.Action;
import com.ehy.order.application.service.interfaces.InventoryService;
import com.ehy.product.dto.InventoryEvent;
import java.util.concurrent.CompletionStage;
import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import lombok.extern.slf4j.Slf4j;
import org.eclipse.microprofile.reactive.messaging.Incoming;

@ApplicationScoped
@Slf4j
public class ProductListener {

  @Inject
  InventoryService inventoryService;

  @Incoming("inventory")
  public CompletionStage<Void> inventoryListener(InventoryEvent inventoryEvent) {
    log.info("[KAFKA] Receive inventory update = {}", inventoryEvent);
    Action action =
        "insert".equals(inventoryEvent.getOperationType()) ? Action.CREATE : Action.UPDATE;
    return inventoryService
        .upsert(inventoryEvent.getFullDocument(), action)
        .onFailure().invoke(t -> log.error("Error listen to inventory changes", t))
        .convert().toCompletionStage()
        .thenAccept(t -> log.info("Inventory updated -> {}", inventoryEvent));
  }
}

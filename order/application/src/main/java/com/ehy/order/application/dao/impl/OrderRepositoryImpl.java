package com.ehy.order.application.dao.impl;

import com.ehy.order.application.dao.interfaces.OrderRepository;
import com.ehy.order.application.entity.domain.Order;
import javax.enterprise.context.ApplicationScoped;

@ApplicationScoped
public class OrderRepositoryImpl extends BaseRepositoryImpl<Order> implements OrderRepository {

}

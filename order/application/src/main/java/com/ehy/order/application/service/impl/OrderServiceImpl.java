package com.ehy.order.application.service.impl;

import com.ehy.common.constant.ResponseCode;
import com.ehy.common.enums.OrderStatus;
import com.ehy.common.exception.BusinessLogicException;
import com.ehy.common.model.BaseDto;
import com.ehy.common.model.MandatoryRequest;
import com.ehy.common.model.order.InventoryDetail;
import com.ehy.order.application.dao.interfaces.BaseRepository;
import com.ehy.order.application.dao.interfaces.OrderRepository;
import com.ehy.order.application.entity.domain.CancellationPolicy;
import com.ehy.order.application.entity.domain.Inventory;
import com.ehy.order.application.entity.domain.Order;
import com.ehy.order.application.entity.enums.Action;
import com.ehy.order.application.service.interfaces.InventoryService;
import com.ehy.order.application.service.interfaces.OrderService;
import com.ehy.order.dto.OrderDetailDto;
import io.smallrye.mutiny.Uni;
import io.smallrye.mutiny.tuples.Tuple2;
import lombok.extern.slf4j.Slf4j;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import java.sql.Date;
import java.text.SimpleDateFormat;
import java.time.Duration;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicReference;
import java.util.stream.Collectors;

@ApplicationScoped
@Slf4j
public class OrderServiceImpl extends BaseResourceServiceImpl<Order> implements OrderService {

  @Inject
  OrderRepository orderRepository;

  @Inject
  InventoryService inventoryService;

  @Override
  protected BaseRepository<Order> repository() {
    return orderRepository;
  }

  @Override
  protected Order mapUpdate(Order oldData, Order newData) {
    return null;
  }

  @Override
  public Uni<String> createOrder(MandatoryRequest mandatoryRequest, Order order) {
    return Uni.createFrom()
        .deferred(() -> validateAllotment(order.getOrderDetail().getInventoryDetails())
            .onItem().produceUni(inv -> create(mandatoryRequest, mapOrder(order, inv))
                .onItem().apply(res -> order.getId().toString())));
  }

  private Uni<List<Inventory>> validateAllotment(
      List<InventoryDetail> inventoryDetails) {
    List<String> inventoryIds = inventoryDetails.stream().map(BaseDto::getId)
        .collect(Collectors.toList());
    return inventoryService.findAllByIdsAndDeleted(inventoryIds, false)
        .onItem().ifNull().failWith(new BusinessLogicException(ResponseCode.DATA_NOT_EXIST,
            Collections.singletonList("Inventory(s) is not available")))
        .onItem().apply(inventories -> {
          Map<String, Inventory> map = new HashMap<>();
          inventories.forEach(inv -> map.put(inv.getId().toString(), inv));
          return Tuple2.of(inventories, map);
        })
        .onItem().produceUni(tuple -> {
          List<Inventory> inventories = tuple.getItem1();
          Map<String, Inventory> inventoryMap = tuple.getItem2();
          if (inventoryDetails.size() != inventoryMap.size()) {
            return Uni.createFrom()
                .failure(new BusinessLogicException(ResponseCode.DATA_NOT_EXIST,
                    Collections.singletonList("One or more inventory is not available")));
          }

          inventoryDetails.forEach(dto -> {
            Inventory inventory = inventoryMap.get(dto.getId());
            if (inventory.getAllotment() - dto.getQuantity() < 1) {
              throw new BusinessLogicException("NOT_ENOUGH_ALLOTMENT",
                  "Remaining allotment after deducted is under 0", Collections.singletonList(
                  "Remaining allotment for inventory id " + dto.getId() + " is under 0"));
            }
          });

          return Uni.createFrom().item(inventories);
        });

  }

  private Order mapOrder(Order order, List<Inventory> inventoryResponse) {
    Map<String, Inventory> inventoryMap = new HashMap<>();
    order.setStatus(OrderStatus.ACTIVE);
    order.setCancellationPolicies(inventoryResponse.stream().map(i -> {
      inventoryMap.put(i.getId().toString(), i);
      return CancellationPolicy.builder()
          .inventoryId(i.getId().toString())
          .daysBefore(i.getCancellationPolicy().getDaysBefore())
          .percentage(i.getCancellationPolicy().getPercentage())
          .refundable(i.getCancellationPolicy().isRefundable())
          .build();
    }).collect(Collectors.toList()));
    order.getOrderDetail().getInventoryDetails().forEach(i -> {
      Inventory inv = inventoryMap.get(i.getId());
      SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
      String formattedDate = formatter.format(Date.from(inv.getDate()));

      i.setDate(formattedDate);
      i.setPrice(inv.getPricing().getPrice());
      i.setAirlineCode(inv.getAirlineCode());
      i.setCurrency(inv.getPricing().getCurrency());
    });

    return order;
  }

  @Override
  public Uni<Boolean> cancel(MandatoryRequest mandatoryRequest, String id) {
    return Uni.createFrom().deferred(() -> findById(id).onItem().ifNull().failWith(
        new BusinessLogicException(ResponseCode.DATA_NOT_EXIST,
            Collections.singletonList(ResponseCode.DATA_NOT_EXIST.getMessage())))
        .onItem().produceUni(order -> {
          if (!OrderStatus.ACTIVE.equals(order.getStatus())) {
            return Uni.createFrom().failure(new BusinessLogicException(ResponseCode.DATA_NOT_EXIST,
                Collections.singletonList("Order is not available")));
          }
          order.setStatus(OrderStatus.CANCELLED);
          return upsert(mandatoryRequest, order, Action.UPDATE);
        }));
  }

  @Override
  public Uni<Boolean> validateRefund(MandatoryRequest mandatoryRequest, String id) {
    return Uni.createFrom().deferred(() -> findById(id).onItem().produceUni(order -> {
      if (!order.getStatus().equals(OrderStatus.CONFIRMED)) {
        return Uni.createFrom()
            .failure(new BusinessLogicException(ResponseCode.BIND_ERROR.getCode(),
                "Order status is not valid",
                Collections.singletonList("Only confirmed order can be refunded or cancelled")));
      }

      Map<String, InventoryDetail> inventoryDetailMap = new HashMap<>();

      AtomicReference<Double> refundAmount = new AtomicReference<>(0D);
      order.getOrderDetail().getInventoryDetails()
          .forEach(i -> inventoryDetailMap.put(i.getId(), i));
      order.getCancellationPolicies().forEach(c -> {
        InventoryDetail inventory = inventoryDetailMap.get(c.getInventoryId());
        LocalDate d1 = LocalDate.now().atStartOfDay().toLocalDate();
        LocalDate d2 = LocalDate.parse(inventory.getDate(), DateTimeFormatter.ISO_DATE);
        Duration diff = Duration.between(d1.atStartOfDay(), d2.atStartOfDay());
        long diffDays = diff.toDays();

        if (c.getDaysBefore() > diffDays) {
          throw new BusinessLogicException("FAILED",
              "Cancellation policy does not meet the criteria",
              Collections.singletonList("Cancellation policy for flight "
                  + inventory.getAirlineCode() + " does not meet the criteria"));
        }
        refundAmount.updateAndGet(v -> v + inventory.getPrice() * c.getPercentage() / 100);
      });

      return Uni.createFrom().item(true);
    }));

  }

  @Override
  public Uni<OrderDetailDto> getOrderDetail(MandatoryRequest mandatoryRequest, String id) {
    return Uni.createFrom().deferred(() -> findById(id).onItem().ifNull().failWith(
            new BusinessLogicException(ResponseCode.DATA_NOT_EXIST,
                    Collections.singletonList(ResponseCode.DATA_NOT_EXIST.getMessage())))
            .onItem().produceUni(order -> Uni.createFrom().item(OrderDetailDto.builder()
                    .orderDetail(order.getOrderDetail())
                    .status(order.getStatus())
                    .cancellationPolicies(order.getCancellationPolicies().stream()
                      .map(cancellationPolicy -> com.ehy.order.dto.domain.CancellationPolicy.builder()
                            .inventoryId(cancellationPolicy.getInventoryId())
                            .refundable(cancellationPolicy.isRefundable())
                            .daysBefore(cancellationPolicy.getDaysBefore())
                            .percentage(cancellationPolicy.getPercentage())
                            .build()).collect(Collectors.toList()))
                    .refundAmount(order.getRefundAmount())
                    .build())));
  }

  @Override
  public Uni<Boolean> changeStatus(MandatoryRequest mandatoryRequest, String id,
      OrderStatus orderStatus) {
    return Uni.createFrom().deferred(() -> findById(id)
        .onItem().produceUni(order -> {
          order.setStatus(orderStatus);
          return upsert(mandatoryRequest, order, Action.UPDATE);
        }).onFailure().invoke(t -> log.error("Error change order status id = {}", id, t)));
  }
}

package com.ehy.order.application.dao.interfaces;

import com.ehy.order.application.entity.domain.SystemParameter;

public interface SystemParameterRepository extends BaseRepository<SystemParameter> {

}

package com.ehy.order.application.entity.constant;

public class CollectionName {

  public static final String SYSTEM_PARAMETER = "system_parameter";
  public static final String ORDER = "order";
  public static final String INVENTORY = "inventory";
}

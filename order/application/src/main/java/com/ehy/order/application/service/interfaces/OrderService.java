package com.ehy.order.application.service.interfaces;

import com.ehy.common.enums.OrderStatus;
import com.ehy.common.model.MandatoryRequest;
import com.ehy.order.application.entity.domain.Order;
import com.ehy.order.dto.OrderDetailDto;
import io.smallrye.mutiny.Uni;

public interface OrderService {

  Uni<String> createOrder(MandatoryRequest mandatoryRequest, Order order);

  Uni<Boolean> changeStatus(MandatoryRequest mandatoryRequest, String id, OrderStatus orderStatus);

  Uni<Boolean> cancel(MandatoryRequest mandatoryRequest, String id);

  Uni<Boolean> validateRefund(MandatoryRequest mandatoryRequest, String id);

  Uni<OrderDetailDto> getOrderDetail(MandatoryRequest mandatoryRequest, String id);
}

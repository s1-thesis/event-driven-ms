package com.ehy.order.application.dao.impl;

import com.ehy.order.application.dao.interfaces.InventoryRepository;
import com.ehy.order.application.entity.domain.Inventory;
import javax.enterprise.context.ApplicationScoped;

@ApplicationScoped
public class InventoryRepositoryImpl extends BaseRepositoryImpl<Inventory> implements
    InventoryRepository {

}

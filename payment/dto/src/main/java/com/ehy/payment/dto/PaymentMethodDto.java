package com.ehy.payment.dto;

import com.ehy.common.enums.PaymentMethodType;
import com.ehy.common.model.BaseDto;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Data
@EqualsAndHashCode(callSuper = true)
@NoArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
public class PaymentMethodDto extends BaseDto<String> {

  @NotBlank
  private String name;
  @NotNull
  private PaymentMethodType paymentMethodType;
  private boolean active;

  @Builder
  public PaymentMethodDto(String id, @NotNull String name,
      @NotBlank PaymentMethodType paymentMethodType, boolean active) {
    super(id);
    this.name = name;
    this.paymentMethodType = paymentMethodType;
    this.active = active;
  }
}

package com.ehy.payment.dto.deserializer;

import com.ehy.payment.dto.PaymentEvent;
import io.quarkus.kafka.client.serialization.ObjectMapperDeserializer;

public class PaymentEventDeserializer extends ObjectMapperDeserializer<PaymentEvent> {

  public PaymentEventDeserializer() {
    super(PaymentEvent.class);
  }
}

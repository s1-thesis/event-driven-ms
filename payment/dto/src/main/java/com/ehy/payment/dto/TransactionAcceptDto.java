package com.ehy.payment.dto;

import com.ehy.common.model.order.Customer;
import com.ehy.common.model.payment.CardInfo;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import javax.validation.constraints.NotBlank;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Data
@EqualsAndHashCode
@AllArgsConstructor
@NoArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
public class TransactionAcceptDto {

  @NotBlank
  private String paymentMethodId;
  private CardInfo cardInfo;
  private Customer billingAddress;
}

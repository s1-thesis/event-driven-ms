# Quarkus Archetype

This project uses Quarkus, the Supersonic Subatomic Java Framework.

If you want to learn more about Quarkus, please visit its website: https://quarkus.io/ .

## Requirements

- Java 8
- Maven 3.6.x
- MongoDB

## How to use archetype

1. Make sure all of the requirements are installed
2. Install the archetype on your local system by typing
`
mvn clean install
` in your terminal
3. Type `mvn archetype:generate` and choose `com.ehy.archetype`
4. You will be prompted to enter groupId, artifactId and version. For example:
```
groupId: com.ehy
artifactId: user
version: 1.0.0-1-SNAPSHOT
```

Adjust your generated application by changing application name in root `pom.xml`, and `application.properties`

## Running the application in dev mode

You can run your application in dev mode that enables live coding using:
```
./mvnw quarkus:dev
```

## Packaging and running the application

The application can be packaged using `./mvnw package`.
It produces the `artifactId-1.0.0-1-SNAPSHOT-runner.jar` file in the `/target` directory.
Be aware that it’s not an _über-jar_ as the dependencies are copied into the `target/lib` directory.

The application is now runnable using `java -jar target/artifactId-1.0.0-1-SNAPSHOT-runner.jar`.

## Creating a native executable

You can create a native executable using: `./mvnw package -Pnative`.

Or, if you don't have GraalVM installed, you can run the native executable build in a container using: `./mvnw package -Pnative -Dquarkus.native.container-build=true`.

You can then execute your native executable with: `./target/artifactid-1.0.0-1-SNAPSHOT-runner`

If you want to learn more about building native executables, please consult https://quarkus.io/guides/building-native-image-guide.
package com.ehy.payment.application.libraries.helper;

import com.ehy.common.model.MandatoryRequest;
import com.ehy.payment.application.entity.domain.BaseMongo;
import com.ehy.payment.application.entity.enums.Action;
import java.time.Instant;
import java.util.Base64;

public class CommonHelper {

  private CommonHelper() {
  }

  public static <T extends BaseMongo> T setBaseMongoFields(MandatoryRequest mandatoryRequest,
      T object, Action action) {
    Instant now = Instant.now();
    switch (action) {
      case CREATE:
        object.setCreatedBy(mandatoryRequest.getUsername());
        object.setUpdatedBy(mandatoryRequest.getUsername());
        object.setCreatedAt(now);
        object.setUpdatedAt(now);
        object.setDeleted(false);
        break;
      case UPDATE:
        object.setUpdatedBy(mandatoryRequest.getUsername());
        object.setUpdatedAt(now);
        object.setDeleted(false);
        break;
      case DELETE:
        object.setUpdatedBy(mandatoryRequest.getUsername());
        object.setUpdatedAt(now);
        object.setDeleted(true);
        break;
    }

    return object;
  }

  public static String buildBasicAuthHeader(String username, String password) {
    return new String(Base64.getEncoder().encode((username + ":" + password).getBytes()));
  }
}

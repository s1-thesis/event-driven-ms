package com.ehy.payment.application.entity.constant;

public class MidtransApiPath {

  public static final String V2 = "/v2";
  public static final String TOKEN = "/token";
  public static final String CHARGE = "/charge";
  public static final String DIRECT_REFUND = "/{id}/refund/online/direct";
}

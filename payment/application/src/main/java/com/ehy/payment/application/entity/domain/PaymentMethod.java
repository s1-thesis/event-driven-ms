package com.ehy.payment.application.entity.domain;

import com.ehy.common.enums.PaymentMethodType;
import com.ehy.payment.application.entity.constant.CollectionName;
import io.quarkus.mongodb.panache.MongoEntity;
import java.time.Instant;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;
import org.bson.types.ObjectId;

@Data
@NoArgsConstructor
@EqualsAndHashCode(callSuper = true)
@ToString(callSuper = true)
@MongoEntity(collection = CollectionName.PAYMENT_METHOD)
public class PaymentMethod extends BaseMongo {

  private PaymentMethodType paymentMethodType;
  private String name;
  private boolean active;

  @Builder
  public PaymentMethod(ObjectId id, String createdBy, String updatedBy, Instant createdAt,
      Instant updatedAt, boolean deleted,
      PaymentMethodType paymentMethodType, String name, boolean active) {
    super(id, createdBy, updatedBy, createdAt, updatedAt, deleted);
    this.paymentMethodType = paymentMethodType;
    this.name = name;
    this.active = active;
  }
}

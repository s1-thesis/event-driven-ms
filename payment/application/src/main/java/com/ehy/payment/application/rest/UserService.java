package com.ehy.payment.application.rest;

import com.ehy.common.configuration.RestLoggingInterceptor;
import com.ehy.common.model.BaseResponse;
import com.ehy.payment.application.entity.constant.ApiPath;
import com.ehy.payment.application.entity.constant.UserApiPath;
import com.ehy.user.dto.UserDto;
import io.smallrye.mutiny.Uni;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import org.eclipse.microprofile.rest.client.annotation.RegisterProvider;
import org.eclipse.microprofile.rest.client.inject.RegisterRestClient;
import org.jboss.resteasy.annotations.jaxrs.HeaderParam;
import org.jboss.resteasy.annotations.jaxrs.PathParam;

@RegisterRestClient
@Produces(MediaType.APPLICATION_JSON)
@RegisterProvider(RestLoggingInterceptor.class)
@Path(UserApiPath.BASE_PATH)
public interface UserService {

  @GET
  @Path(UserApiPath.APPEND_USERS + ApiPath.APPEND_ID)
  Uni<BaseResponse<UserDto>> findUserById(@HeaderParam String currency,
      @HeaderParam String clientIpAddress,
      @HeaderParam String clientSessionId,
      @HeaderParam String requestId,
      @HeaderParam String serviceId,
      @HeaderParam String username,
      @PathParam("id") String id);
}

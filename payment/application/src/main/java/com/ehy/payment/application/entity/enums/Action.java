package com.ehy.payment.application.entity.enums;

public enum Action {
  CREATE, UPDATE, DELETE
}

package com.ehy.payment.application.web.controller;

import com.ehy.common.model.BaseResponse;
import com.ehy.common.model.MandatoryRequest;
import com.ehy.common.model.PageWrapper;
import com.ehy.payment.application.entity.constant.ApiPath;
import com.ehy.payment.application.libraries.mapper.PaymentMethodMapper;
import com.ehy.payment.application.service.interfaces.PaymentMethodService;
import com.ehy.payment.dto.PaymentMethodDto;
import io.smallrye.mutiny.Uni;
import java.util.List;
import javax.inject.Inject;
import javax.validation.Valid;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.ws.rs.BeanParam;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import org.eclipse.microprofile.openapi.annotations.parameters.RequestBody;
import org.eclipse.microprofile.openapi.annotations.tags.Tag;
import org.eclipse.microprofile.openapi.annotations.tags.Tags;
import org.jboss.resteasy.annotations.jaxrs.PathParam;
import org.jboss.resteasy.annotations.jaxrs.QueryParam;

@Path(ApiPath.BASE_PATH_PAYMENT_METHODS)
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
@Tags(value = @Tag(name = "Payment Method", description = "CRUD for payment method"))
public class PaymentMethodController extends BaseResource {

  @Inject
  PaymentMethodService paymentMethodService;
  
  @Inject
  PaymentMethodMapper mapper;
  
  @GET
  public Uni<BaseResponse<PageWrapper<PaymentMethodDto>>> findAll(
      @BeanParam @Valid MandatoryRequest mandatoryRequest,
      @Valid @Min(0) @DefaultValue("0") @NotNull @QueryParam Integer page,
      @Valid @Min(0) @DefaultValue("10") @Max(100) @NotNull @QueryParam Integer size) {
    return buildResponse(paymentMethodService.findAll(page, size)
        .onItem().apply(pm -> {
          List<PaymentMethodDto> paymentMethodDtoList = mapper.toDtoList(pm.getContent());
          return PageWrapper.<PaymentMethodDto>builder().pageInfo(pm.getPageInfo())
              .content(paymentMethodDtoList).build();
        }));
  }

  @GET
  @Path(ApiPath.APPEND_ID)
  public Uni<BaseResponse<PaymentMethodDto>> findOne(
      @BeanParam @Valid MandatoryRequest mandatoryRequest,
      @PathParam String id) {
    return buildResponse(
        paymentMethodService.findById(id).onItem().apply(item -> mapper.toDto(item)));
  }

  @POST
  public Uni<BaseResponse<Boolean>> create(
      @BeanParam @Valid MandatoryRequest mandatoryRequest,
      @RequestBody @Valid PaymentMethodDto paymentMethod) {
    return buildResponse(
        paymentMethodService.create(mandatoryRequest, mapper.toDomain(paymentMethod)));
  }

  @PUT
  @Path(ApiPath.APPEND_ID)
  public Uni<BaseResponse<Boolean>> update(
      @BeanParam @Valid MandatoryRequest mandatoryRequest,
      @PathParam String id,
      @RequestBody @Valid PaymentMethodDto paymentMethod) {
    paymentMethod.setId(id);
    return buildResponse(
        paymentMethodService.update(mandatoryRequest, mapper.toDomain(paymentMethod)));
  }

  @DELETE
  @Path(ApiPath.APPEND_ID)
  public Uni<BaseResponse<Boolean>> delete(
      @BeanParam @Valid MandatoryRequest mandatoryRequest,
      @PathParam String id) {
    return buildResponse(paymentMethodService.delete(mandatoryRequest, id));
  }
}

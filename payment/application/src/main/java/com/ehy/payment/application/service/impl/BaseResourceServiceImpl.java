package com.ehy.payment.application.service.impl;

import com.ehy.common.constant.ResponseCode;
import com.ehy.common.exception.BusinessLogicException;
import com.ehy.common.model.MandatoryRequest;
import com.ehy.common.model.PageInfo;
import com.ehy.common.model.PageWrapper;
import com.ehy.payment.application.dao.interfaces.BaseRepository;
import com.ehy.payment.application.entity.domain.BaseMongo;
import com.ehy.payment.application.entity.enums.Action;
import com.ehy.payment.application.service.interfaces.BaseResourceService;
import io.quarkus.mongodb.panache.reactive.ReactivePanacheQuery;
import io.quarkus.panache.common.Page;
import io.smallrye.mutiny.Uni;
import java.lang.reflect.ParameterizedType;
import java.util.Collections;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public abstract class BaseResourceServiceImpl<T extends BaseMongo> implements
    BaseResourceService<T> {

  protected abstract BaseRepository<T> repository();

  protected abstract T mapUpdate(T oldData, T newData);

  @Override
  public Uni<PageWrapper<T>> findAll(Integer page, Integer size) {
    return Uni.createFrom().deferred(() -> repository().countByDeleted(false)
        .onItem().produceUni(totalData -> {
          ReactivePanacheQuery<T> q = repository().findAllByDeleted(false, Page.of(page, size));
          return Uni.combine().all().unis(q.count(), q.list(), q.pageCount())
              .asTuple().onItem().apply(tuple -> PageWrapper.<T>builder()
                  .pageInfo(PageInfo.builder()
                      .dataCount(size).pageIndex(page).pageCount(tuple.getItem3())
                      .totalData(tuple.getItem1())
                      .build())
                  .content(tuple.getItem2())
                  .build());
        })
    );
  }

  @Override
  public Uni<T> findById(String id) {
    return Uni.createFrom().deferred(() ->
        repository().findByIdAndDeleted(id, false).onItem().ifNull()
            .failWith(new BusinessLogicException(ResponseCode.DATA_NOT_EXIST.getCode(),
                ResponseCode.DATA_NOT_EXIST.getMessage(),
                Collections.singletonList(ResponseCode.DATA_NOT_EXIST.getMessage())))
            .onFailure()
            .invoke(t -> log.error("Error find {}, id = {}", getParameterizedTypeName(), id, t)));
  }

  @Override
  public Uni<Boolean> create(MandatoryRequest mandatoryRequest, T object) {
    return Uni.createFrom().deferred(() -> {
      object.setId(null);
      return upsert(mandatoryRequest, object, Action.CREATE);
    });
  }

  @Override
  public Uni<Boolean> update(MandatoryRequest mandatoryRequest, T object) {
    return Uni.createFrom().deferred(() -> findById(object.getId().toString()).onItem()
        .produceUni(data -> upsert(mandatoryRequest, mapUpdate(data, object), Action.UPDATE)));
  }

  @Override
  public Uni<Boolean> delete(MandatoryRequest mandatoryRequest, String id) {
    return Uni.createFrom().deferred(() -> findById(id).onItem()
        .produceUni(data -> upsert(mandatoryRequest, data, Action.DELETE)));
  }

  protected Uni<Boolean> upsert(MandatoryRequest mandatoryRequest, T object,
      Action action) {
    return Uni.createFrom()
        .deferred(() -> repository().upsert(mandatoryRequest, object, action)
            .onItem().apply(t -> true))
        .onFailure().invoke(t -> log.error("Error {} {}, object = {}",
            action.name(), getParameterizedTypeName(), object, t));
  }

  protected String getParameterizedTypeName() {
    return ((ParameterizedType) getClass().getGenericSuperclass())
        .getActualTypeArguments()[0].getTypeName();
  }
}

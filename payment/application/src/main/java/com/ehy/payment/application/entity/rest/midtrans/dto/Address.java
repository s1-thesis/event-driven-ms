package com.ehy.payment.application.entity.rest.midtrans.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class Address {

  private String firstName;
  private String lastName;
  private String email;
  private String phone;
  private String address;
  private String city;
  @JsonProperty("postal_code")
  private String postalCode;
  @JsonProperty("country_code")
  private String countryCode;
}

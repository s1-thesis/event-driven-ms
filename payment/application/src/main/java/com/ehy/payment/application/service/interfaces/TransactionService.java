package com.ehy.payment.application.service.interfaces;

import com.ehy.common.model.MandatoryRequest;
import com.ehy.common.model.order.Customer;
import com.ehy.common.model.order.OrderDetail;
import com.ehy.common.model.payment.CardInfo;
import com.ehy.payment.application.entity.domain.Transaction;
import com.ehy.payment.application.entity.rest.midtrans.response.ChargeResponse;
import io.smallrye.mutiny.Uni;

public interface TransactionService extends BaseResourceService<Transaction> {

  Uni<String> create(MandatoryRequest mandatoryRequest, OrderDetail order);

  Uni<String> accept(MandatoryRequest mandatoryRequest, String id, String paymentMethodId,
      CardInfo cardInfo, Customer billingAddress);

  Uni<String> confirmPayment(ChargeResponse confirmRequest);

  Uni<Boolean> cancel(MandatoryRequest mandatoryRequest, String orderId);

  Uni<Boolean> refund(MandatoryRequest mandatoryRequest, String orderId);
}

package com.ehy.payment.application.service.interfaces;

import com.ehy.payment.application.entity.domain.PaymentMethod;

public interface PaymentMethodService extends BaseResourceService<PaymentMethod> {

}

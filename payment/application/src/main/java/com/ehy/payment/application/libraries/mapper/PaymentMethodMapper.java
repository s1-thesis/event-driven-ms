package com.ehy.payment.application.libraries.mapper;

import com.ehy.payment.application.entity.domain.PaymentMethod;
import com.ehy.payment.dto.PaymentMethodDto;
import java.util.List;
import org.mapstruct.IterableMapping;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Named;

@Mapper(componentModel = "cdi")
public interface PaymentMethodMapper extends BaseMapper {

  @Named("toDto")
  @Mapping(target = "id", source = "paymentMethod.id", qualifiedByName = "objectIdToString")
  PaymentMethodDto toDto(PaymentMethod paymentMethod);

  @Mapping(target = "id", source = "paymentMethodDto.id", qualifiedByName = "stringToObjectId")
  PaymentMethod toDomain(PaymentMethodDto paymentMethodDto);

  @IterableMapping(qualifiedByName = "toDto")
  List<PaymentMethodDto> toDtoList(List<PaymentMethod> paymentMethods);
}

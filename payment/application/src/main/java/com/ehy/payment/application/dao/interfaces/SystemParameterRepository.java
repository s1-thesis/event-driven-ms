package com.ehy.payment.application.dao.interfaces;

import com.ehy.payment.application.entity.domain.SystemParameter;

public interface SystemParameterRepository extends BaseRepository<SystemParameter> {

}

package com.ehy.payment.application.entity.rest.midtrans.response;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
public abstract class BaseResponse {

  @JsonProperty("status_code")
  private String statusCode;
  @JsonProperty("status_message")
  private String statusMessage;
}

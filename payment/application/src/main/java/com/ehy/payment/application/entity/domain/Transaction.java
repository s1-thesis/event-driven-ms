package com.ehy.payment.application.entity.domain;

import com.ehy.common.enums.PaymentMethodType;
import com.ehy.common.enums.PaymentStatus;
import com.ehy.common.model.order.Customer;
import com.ehy.common.model.order.OrderDetail;
import com.ehy.common.model.payment.CardInfo;
import com.ehy.payment.application.entity.constant.CollectionName;
import com.ehy.payment.application.entity.rest.midtrans.request.ChargeRequest;
import com.ehy.payment.application.entity.rest.midtrans.response.ChargeResponse;
import io.quarkus.mongodb.panache.MongoEntity;
import java.time.Instant;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;
import org.bson.types.ObjectId;

@Data
@NoArgsConstructor
@EqualsAndHashCode(callSuper = true)
@ToString(callSuper = true)
@MongoEntity(collection = CollectionName.TRANSACTION)
public class Transaction extends BaseMongo {

  private String orderId;
  private String midtransTransactionId;
  private OrderDetail orderDetail;
  private PaymentStatus status;
  private PaymentMethodType paymentMethodType;
  private String paymentMethodName;
  private CardInfo cardInfo;
  private Customer billingAddress;
  private ChargeRequest request;
  private ChargeResponse response;

  @Builder
  public Transaction(ObjectId id, String createdBy, String updatedBy, Instant createdAt,
      Instant updatedAt, boolean deleted, String orderId, String midtransTransactionId,
      OrderDetail orderDetail, PaymentStatus status,
      PaymentMethodType paymentMethodType, String paymentMethodName,
      CardInfo cardInfo, Customer billingAddress,
      ChargeRequest request,
      ChargeResponse response) {
    super(id, createdBy, updatedBy, createdAt, updatedAt, deleted);
    this.orderId = orderId;
    this.midtransTransactionId = midtransTransactionId;
    this.orderDetail = orderDetail;
    this.status = status;
    this.paymentMethodType = paymentMethodType;
    this.paymentMethodName = paymentMethodName;
    this.cardInfo = cardInfo;
    this.billingAddress = billingAddress;
    this.request = request;
    this.response = response;
  }
}

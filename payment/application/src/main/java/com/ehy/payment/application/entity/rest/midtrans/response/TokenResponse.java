package com.ehy.payment.application.entity.rest.midtrans.response;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Builder;
import lombok.Data;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class TokenResponse extends BaseResponse {

  @JsonProperty("token_id")
  private String tokenId;
  private String hash;

  @Builder
  public TokenResponse(String statusCode, String statusMessage, String tokenId, String hash) {
    super(statusCode, statusMessage);
    this.tokenId = tokenId;
    this.hash = hash;
  }
}

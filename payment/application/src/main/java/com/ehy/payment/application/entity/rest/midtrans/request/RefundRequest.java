package com.ehy.payment.application.entity.rest.midtrans.request;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class RefundRequest {

  @JsonProperty("refund_key")
  private String refundKey;

  private Double amount;

  private String reason;
}

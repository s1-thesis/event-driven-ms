package com.ehy.payment.application.entity.constant;

public class UserApiPath {

  public static final String BASE_PATH = "/user-service";
  public static final String APPEND_USERS = "/users";
}

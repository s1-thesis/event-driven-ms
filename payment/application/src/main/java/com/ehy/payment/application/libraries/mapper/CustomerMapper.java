package com.ehy.payment.application.libraries.mapper;

import com.ehy.common.model.order.Customer;
import com.ehy.payment.application.entity.rest.midtrans.dto.Address;
import org.mapstruct.Mapper;

@Mapper(componentModel = "cdi")
public interface CustomerMapper {

  Address toAddress(Customer customer);
}

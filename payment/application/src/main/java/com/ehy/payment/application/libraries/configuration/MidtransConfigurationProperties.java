package com.ehy.payment.application.libraries.configuration;

import io.quarkus.arc.config.ConfigProperties;
import lombok.Getter;
import lombok.Setter;

@ConfigProperties(prefix = "midtrans")
@Getter
@Setter
public class MidtransConfigurationProperties {

  private String clientKey;
  private String serverKey;
}

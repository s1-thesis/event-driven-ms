package com.ehy.payment.application.dao.impl;

import com.ehy.payment.application.dao.interfaces.PaymentMethodRepository;
import com.ehy.payment.application.entity.domain.PaymentMethod;
import javax.enterprise.context.ApplicationScoped;

@ApplicationScoped
public class PaymentMethodRepositoryImpl extends BaseRepositoryImpl<PaymentMethod> implements
    PaymentMethodRepository {

}

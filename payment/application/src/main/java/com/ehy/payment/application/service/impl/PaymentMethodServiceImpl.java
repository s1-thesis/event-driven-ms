package com.ehy.payment.application.service.impl;

import com.ehy.payment.application.dao.interfaces.BaseRepository;
import com.ehy.payment.application.dao.interfaces.PaymentMethodRepository;
import com.ehy.payment.application.entity.domain.PaymentMethod;
import com.ehy.payment.application.service.interfaces.PaymentMethodService;
import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;

@ApplicationScoped
public class PaymentMethodServiceImpl extends BaseResourceServiceImpl<PaymentMethod> implements PaymentMethodService {

  @Inject
  PaymentMethodRepository paymentMethodRepository;

  @Override
  protected BaseRepository<PaymentMethod> repository() {
    return paymentMethodRepository;
  }

  @Override
  protected PaymentMethod mapUpdate(PaymentMethod oldData, PaymentMethod newData) {
    newData.setId(oldData.getId());
    newData.setCreatedAt(oldData.getCreatedAt());
    newData.setCreatedBy(oldData.getCreatedBy());
    return newData;
  }
}

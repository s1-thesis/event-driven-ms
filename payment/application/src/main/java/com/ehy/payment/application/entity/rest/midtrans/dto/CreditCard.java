package com.ehy.payment.application.entity.rest.midtrans.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class CreditCard {

  @JsonProperty("token_id")
  private String tokenId;
  private boolean authentication;
}

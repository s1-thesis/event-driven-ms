package com.ehy.payment.application.entity.rest.midtrans.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class ItemDetail {

  private String id;
  private Double price;
  private Integer quantity;
  private String name;
}

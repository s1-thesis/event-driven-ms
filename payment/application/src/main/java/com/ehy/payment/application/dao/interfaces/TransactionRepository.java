package com.ehy.payment.application.dao.interfaces;

import com.ehy.payment.application.entity.domain.Transaction;

public interface TransactionRepository extends BaseRepository<Transaction> {

}

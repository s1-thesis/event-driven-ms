package com.ehy.payment.application.entity.rest.midtrans.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class TransactionDetail {

  @JsonProperty("order_id")
  private String orderId;

  @JsonProperty("gross_amount")
  private Double grossAmount;
}

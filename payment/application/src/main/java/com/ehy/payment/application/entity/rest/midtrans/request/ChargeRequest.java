package com.ehy.payment.application.entity.rest.midtrans.request;

import com.ehy.payment.application.entity.rest.midtrans.dto.BankTransfer;
import com.ehy.payment.application.entity.rest.midtrans.dto.CreditCard;
import com.ehy.payment.application.entity.rest.midtrans.dto.CustomerDetail;
import com.ehy.payment.application.entity.rest.midtrans.dto.Gopay;
import com.ehy.payment.application.entity.rest.midtrans.dto.ItemDetail;
import com.ehy.payment.application.entity.rest.midtrans.dto.TransactionDetail;
import com.fasterxml.jackson.annotation.JsonProperty;
import java.util.List;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class ChargeRequest {

  @JsonProperty("payment_type")
  private String paymentType;

  @JsonProperty("transaction_details")
  private TransactionDetail transactionDetail;

  @JsonProperty("item_details")
  private List<ItemDetail> itemDetails;

  @JsonProperty("customer_details")
  private CustomerDetail customerDetail;

  @JsonProperty("bank_transfer")
  private BankTransfer bankTransfer;

  @JsonProperty("credit_card")
  private CreditCard creditCard;

  private Gopay gopay;
}

package com.ehy.payment.application.service.interfaces;

import com.ehy.payment.application.entity.domain.SystemParameter;

public interface SystemParameterService extends BaseResourceService<SystemParameter> {

}

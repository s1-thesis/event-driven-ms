package com.ehy.payment.application.entity.constant;

public class ApiPath {

  /**
   * Rename this with service name
   */
  public static final String BASE_PATH = "/payment-service";

  public static final String APPEND_SYSTEM_PARAMETERS = "/system-parameters";
  public static final String APPEND_ID = "/{id}";
  public static final String APPEND_PAYMENT_METHOD = "/payment-methods";
  public static final String APPEND_TRANSACTION = "/transactions";
  public static final String APPEND_ACCEPT = "/accept";
  public static final String APPEND_CONFIRM = "/confirm";
  public static final String APPEND_CANCEL = "/cancel";
  public static final String APPEND_REFUND = "/refund";

  /**
   * Base path
   */
  public static final String BASE_PATH_SYSTEM_PARAMETERS = BASE_PATH + APPEND_SYSTEM_PARAMETERS;
  public static final String BASE_PATH_PAYMENT_METHODS = BASE_PATH + APPEND_PAYMENT_METHOD;
  public static final String BASE_PATH_TRANSACTIONS = BASE_PATH + APPEND_TRANSACTION;
}

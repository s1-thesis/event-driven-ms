package com.ehy.payment.application.entity.constant;

public class BaseMongoFields {

  public static final String ID = "_id";
  public static final String CREATED_BY = "createdBy";
  public static final String UPDATED_BY = "updatedBy";
  public static final String CREATED_AT = "createdAt";
  public static final String UPDATED_AT = "updatedAt";
  public static final String DELETED = "deleted";
}

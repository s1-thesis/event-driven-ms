package com.ehy.payment.application.dao.interfaces;

import com.ehy.payment.application.entity.domain.PaymentMethod;

public interface PaymentMethodRepository extends BaseRepository<PaymentMethod> {

}

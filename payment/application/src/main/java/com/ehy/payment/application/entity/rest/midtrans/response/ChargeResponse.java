package com.ehy.payment.application.entity.rest.midtrans.response;

import com.ehy.payment.application.entity.rest.midtrans.dto.BankTransfer;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import java.util.List;
import lombok.Data;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class ChargeResponse {

  @JsonProperty("status_code")
  private String statusCode;

  @JsonProperty("status_message")
  private String statusMessage;

  @JsonProperty("validation_messages")
  private List<String> validationMessages;

  @JsonProperty("transaction_id")
  private String transactionId;

  @JsonProperty("order_id")
  private String orderId;

  @JsonProperty("gross_amount")
  private String grossAmount;

  private String currency;

  @JsonProperty("payment_type")
  private String paymentType;

  @JsonProperty("transaction_status")
  private String transactionStatus;

  @JsonProperty("transaction_date")
  private String transactionDate;

  @JsonProperty("fraud_status")
  private String fraudStatus;

  @JsonProperty("permata_va_number")
  private String permataVaNumber;

  @JsonProperty("merchant_id")
  private String merchantId;

  @JsonProperty("va_numbers")
  private List<BankTransfer> vaNumbers;

  @JsonProperty("redirect_url")
  private String redirectUrl;
}

package com.ehy.payment.application.listener.kafka;

import com.ehy.common.constant.ConstantValues;
import com.ehy.common.constant.ServiceId;
import com.ehy.common.enums.OrderStatus;
import com.ehy.common.model.MandatoryRequest;
import com.ehy.order.dto.OrderEvent;
import com.ehy.order.dto.domain.Order;
import com.ehy.payment.application.service.interfaces.TransactionService;
import io.smallrye.mutiny.Uni;
import java.util.concurrent.CompletionStage;
import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import lombok.extern.slf4j.Slf4j;
import org.eclipse.microprofile.reactive.messaging.Incoming;

@ApplicationScoped
@Slf4j
public class OrderListener {

  @Inject
  TransactionService transactionService;

  @Incoming("order")
  public CompletionStage<Void> handleOrderEvent(OrderEvent orderEvent) {
    log.info("[KAFKA] Receive order event -> {}", orderEvent);

    Order order = orderEvent.getFullDocument();


    if (OrderStatus.ACTIVE.equals(order.getStatus())) {
      orderEvent.getFullDocument().getOrderDetail().setId(order.getId());
      return transactionService
          .create(MandatoryRequest.builder().username(ConstantValues.SYSTEM)
                  .serviceId(ServiceId.PAYMENT_SERVICE).requestId(ConstantValues.REQUEST_ID)
                  .clientSessionId(ConstantValues.CUSTOMER_SESSION_ID_INTERNAL)
                  .clientIpAddress(ConstantValues.CUSTOMER_IP_ADDRESS_INTERNAL)
                  .currency(ConstantValues.CURRENCY_IDR).build(),
              orderEvent.getFullDocument().getOrderDetail())
          .onFailure().invoke(t -> log.error("Error creating transaction, order id -> {}", order.getId(), t))
          .subscribeAsCompletionStage()
          .thenAccept(t -> log.info("Transaction created for order -> {}", order.getId()));
    } else if (OrderStatus.EXPIRED.equals(order.getStatus()) || OrderStatus.CANCELLED
        .equals(order.getStatus())) {
      return transactionService
          .cancel(MandatoryRequest.builder().username(order.getUpdatedBy()).build(), order.getId())
          .onFailure().invoke(t -> log.error("Error cancelling transaction, order id -> {}", order.getId(), t))
          .subscribeAsCompletionStage()
          .thenAccept(t -> log.info("Transaction created for order -> {}", order.getId()));
    }

    return Uni.createFrom().<Void>nothing().subscribeAsCompletionStage();
  }
}

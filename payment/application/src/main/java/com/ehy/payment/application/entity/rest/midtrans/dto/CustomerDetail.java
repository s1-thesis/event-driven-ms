package com.ehy.payment.application.entity.rest.midtrans.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class CustomerDetail {

  private String firstName;
  private String lastName;
  private String email;
  private String phone;
  @JsonProperty("billing_address")
  private Address billingAddress;
  @JsonProperty("shipping_address")
  private Address shippingAddress;
}

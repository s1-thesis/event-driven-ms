package com.ehy.payment.application.dao.impl;

import com.ehy.payment.application.dao.interfaces.TransactionRepository;
import com.ehy.payment.application.entity.domain.Transaction;
import javax.enterprise.context.ApplicationScoped;

@ApplicationScoped
public class TransactionRepositoryImpl extends BaseRepositoryImpl<Transaction> implements
    TransactionRepository {

}

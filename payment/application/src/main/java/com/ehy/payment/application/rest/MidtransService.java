package com.ehy.payment.application.rest;

import com.ehy.common.configuration.RestLoggingInterceptor;
import com.ehy.payment.application.entity.constant.MidtransApiPath;
import com.ehy.payment.application.entity.rest.midtrans.request.ChargeRequest;
import com.ehy.payment.application.entity.rest.midtrans.request.RefundRequest;
import com.ehy.payment.application.entity.rest.midtrans.response.ChargeResponse;
import com.ehy.payment.application.entity.rest.midtrans.response.TokenResponse;
import io.smallrye.mutiny.Uni;
import javax.ws.rs.GET;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import org.eclipse.microprofile.rest.client.annotation.RegisterProvider;
import org.eclipse.microprofile.rest.client.inject.RegisterRestClient;
import org.jboss.resteasy.annotations.jaxrs.PathParam;
import org.jboss.resteasy.annotations.jaxrs.QueryParam;

@Path(MidtransApiPath.V2)
@Produces(MediaType.APPLICATION_JSON)
@RegisterProvider(RestLoggingInterceptor.class)
@RegisterRestClient
public interface MidtransService {

  @GET
  @Path(MidtransApiPath.TOKEN)
  Uni<TokenResponse> retrieveToken(@QueryParam("client_key") String clientKey,
      @QueryParam("card_number") String cardNumber,
      @QueryParam("card_exp_month") String expMonth,
      @QueryParam("card_exp_year") String expYear, @QueryParam("card_cvv") String cvv);

  @POST
  @Path(MidtransApiPath.CHARGE)
  Uni<ChargeResponse> charge(@HeaderParam("Authorization") String authKey, ChargeRequest chargeRequest);

  @POST
  @Path(MidtransApiPath.DIRECT_REFUND)
  Uni<ChargeResponse> refund(@HeaderParam("Authorization") String authKey, @PathParam("id") String id, RefundRequest refundRequest);
}

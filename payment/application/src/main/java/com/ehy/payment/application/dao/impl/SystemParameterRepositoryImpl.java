package com.ehy.payment.application.dao.impl;

import com.ehy.payment.application.dao.interfaces.SystemParameterRepository;
import com.ehy.payment.application.entity.domain.SystemParameter;
import javax.enterprise.context.ApplicationScoped;

@ApplicationScoped
public class SystemParameterRepositoryImpl extends BaseRepositoryImpl<SystemParameter> implements SystemParameterRepository {

}

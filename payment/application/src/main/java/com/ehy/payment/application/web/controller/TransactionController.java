package com.ehy.payment.application.web.controller;

import com.ehy.common.model.BaseResponse;
import com.ehy.common.model.MandatoryRequest;
import com.ehy.payment.application.entity.constant.ApiPath;
import com.ehy.payment.application.entity.rest.midtrans.response.ChargeResponse;
import com.ehy.payment.application.service.interfaces.TransactionService;
import com.ehy.payment.dto.TransactionAcceptDto;
import io.smallrye.mutiny.Uni;
import javax.inject.Inject;
import javax.validation.Valid;
import javax.ws.rs.BeanParam;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import org.eclipse.microprofile.openapi.annotations.tags.Tag;
import org.eclipse.microprofile.openapi.annotations.tags.Tags;
import org.jboss.resteasy.annotations.jaxrs.PathParam;

@Path(ApiPath.BASE_PATH_TRANSACTIONS)
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
@Tags(value = @Tag(name = "Payment Transaction", description = "Payment transaction endpoints"))
public class TransactionController extends BaseResource {

  @Inject
  TransactionService transactionService;

  /**
   * User will be prompted to choose payment method after order is placed
   *
   * @param mandatoryRequest mandatory header
   * @param id order id of the transaction
   * @param transactionDto includes card info, payment method id, and billing address
   * @return VA number or payment redirect link
   */
  @POST
  @Path(ApiPath.APPEND_ID + ApiPath.APPEND_ACCEPT)
  public Uni<BaseResponse<String>> accept(
      @BeanParam @Valid MandatoryRequest mandatoryRequest,
      @PathParam String id,
      @Valid TransactionAcceptDto transactionDto) {
    return buildResponse(transactionService
        .accept(mandatoryRequest, id, transactionDto.getPaymentMethodId(),
            transactionDto.getCardInfo(), transactionDto.getBillingAddress()));
  }

  /**
   * This endpoint will be confirmed only by midtrans
   *
   * @param confirmRequest is basically response from midtrans' accept payment
   * @return transaction status
   */
  @POST
  @Path(ApiPath.APPEND_CONFIRM)
  public Uni<BaseResponse<String>> confirmPayment(ChargeResponse confirmRequest) {
    return buildResponse(transactionService.confirmPayment(confirmRequest));
  }

  /**
   * Endpoint to refund payment and order
   *
   * @param mandatoryRequest mandatory header
   * @param id order id of transaction
   * @return boolean
   */
  @POST
  @Path(ApiPath.APPEND_ID + ApiPath.APPEND_REFUND)
  public Uni<BaseResponse<Boolean>> refund(
      @BeanParam @Valid MandatoryRequest mandatoryRequest,
      @PathParam String id) {
    return buildResponse(transactionService.refund(mandatoryRequest, id));
  }
}

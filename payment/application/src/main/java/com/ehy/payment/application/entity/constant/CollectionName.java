package com.ehy.payment.application.entity.constant;

public class CollectionName {

  public static final String SYSTEM_PARAMETER = "system_parameter";
  public static final String PAYMENT_METHOD = "payment_method";
  public static final String TRANSACTION = "transaction";
}

package com.ehy.payment.application.entity.constant;

public class OrderApiPath {

  public static final String BASE_PATH = "/order-service";
  public static final String ORDERS = "/orders";
  public static final String CONFIRM = "/confirm";
  public static final String REJECT = "/reject";
  public static final String VALIDATE = "/validate";
  public static final String REFUND = "/refund";
}

package com.ehy.payment.application.rest;

import com.ehy.common.configuration.RestLoggingInterceptor;
import com.ehy.common.model.BaseResponse;
import com.ehy.payment.application.entity.constant.ApiPath;
import com.ehy.payment.application.entity.constant.OrderApiPath;
import io.smallrye.mutiny.Uni;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import org.eclipse.microprofile.rest.client.annotation.RegisterProvider;
import org.eclipse.microprofile.rest.client.inject.RegisterRestClient;
import org.jboss.resteasy.annotations.jaxrs.HeaderParam;
import org.jboss.resteasy.annotations.jaxrs.PathParam;

@Path(OrderApiPath.BASE_PATH)
@Produces(MediaType.APPLICATION_JSON)
@RegisterProvider(RestLoggingInterceptor.class)
@RegisterRestClient
public interface OrderService {

  @POST
  @Path(OrderApiPath.ORDERS + ApiPath.APPEND_ID + OrderApiPath.REFUND + OrderApiPath.VALIDATE)
  Uni<BaseResponse<Boolean>> validateRefund(@HeaderParam String currency,
      @HeaderParam String clientIpAddress,
      @HeaderParam String clientSessionId,
      @HeaderParam String requestId,
      @HeaderParam String serviceId,
      @HeaderParam String username, @PathParam String id);
}

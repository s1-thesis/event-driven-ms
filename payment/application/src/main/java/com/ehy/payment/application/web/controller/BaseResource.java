package com.ehy.payment.application.web.controller;

import com.ehy.common.constant.ResponseCode;
import com.ehy.common.helper.BaseResponseHelper;
import com.ehy.common.model.BaseResponse;
import io.smallrye.mutiny.Uni;

@SuppressWarnings("unchecked")
public abstract class BaseResource {

  public <T> Uni<BaseResponse<T>> buildResponse(Uni<T> uni) {
    return Uni.createFrom().deferred(() -> uni.onItem().apply(result -> BaseResponseHelper
        .build(ResponseCode.SUCCESS.getCode(),
            ResponseCode.SUCCESS.getMessage(), result, null)));
  }
}

package com.ehy.payment.application.service.impl;

import com.ehy.common.constant.ResponseCode;
import com.ehy.common.constant.ServiceId;
import com.ehy.common.enums.PaymentMethodType;
import com.ehy.common.enums.PaymentStatus;
import com.ehy.common.exception.BusinessLogicException;
import com.ehy.common.model.BaseResponse;
import com.ehy.common.model.MandatoryRequest;
import com.ehy.common.model.order.Customer;
import com.ehy.common.model.order.OrderDetail;
import com.ehy.common.model.payment.CardInfo;
import com.ehy.payment.application.dao.interfaces.BaseRepository;
import com.ehy.payment.application.dao.interfaces.TransactionRepository;
import com.ehy.payment.application.entity.domain.Transaction;
import com.ehy.payment.application.entity.enums.Action;
import com.ehy.payment.application.entity.rest.midtrans.dto.BankTransfer;
import com.ehy.payment.application.entity.rest.midtrans.dto.CreditCard;
import com.ehy.payment.application.entity.rest.midtrans.dto.CustomerDetail;
import com.ehy.payment.application.entity.rest.midtrans.dto.ItemDetail;
import com.ehy.payment.application.entity.rest.midtrans.dto.TransactionDetail;
import com.ehy.payment.application.entity.rest.midtrans.request.ChargeRequest;
import com.ehy.payment.application.entity.rest.midtrans.request.RefundRequest;
import com.ehy.payment.application.entity.rest.midtrans.response.ChargeResponse;
import com.ehy.payment.application.libraries.configuration.MidtransConfigurationProperties;
import com.ehy.payment.application.libraries.helper.CommonHelper;
import com.ehy.payment.application.libraries.mapper.CustomerMapper;
import com.ehy.payment.application.rest.MidtransService;
import com.ehy.payment.application.rest.OrderService;
import com.ehy.payment.application.rest.UserService;
import com.ehy.payment.application.service.interfaces.PaymentMethodService;
import com.ehy.payment.application.service.interfaces.TransactionService;
import io.smallrye.mutiny.Uni;
import java.util.Arrays;
import java.util.Collections;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Collectors;
import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import lombok.extern.slf4j.Slf4j;
import org.eclipse.microprofile.rest.client.inject.RestClient;

@ApplicationScoped
@Slf4j
public class TransactionServiceImpl extends BaseResourceServiceImpl<Transaction> implements
    TransactionService {

  @Inject
  TransactionRepository transactionRepository;

  @Inject
  PaymentMethodService paymentMethodService;

  @Inject
  MidtransConfigurationProperties midtransProperties;

  @Inject
  @RestClient
  MidtransService midtransService;

  @Inject
  @RestClient
  OrderService orderService;

  @Inject
  @RestClient
  UserService userService;

  @Inject
  CustomerMapper customerMapper;

  @Override
  protected BaseRepository<Transaction> repository() {
    return transactionRepository;
  }

  @Override
  protected Transaction mapUpdate(Transaction oldData, Transaction newData) {
    return null;
  }

  @Override
  public Uni<String> create(MandatoryRequest mandatoryRequest, OrderDetail order) {
    return Uni.createFrom()
        .deferred(() -> userService.findUserById(mandatoryRequest.getCurrency(),
            mandatoryRequest.getClientIpAddress(),
            mandatoryRequest.getClientSessionId(),
            mandatoryRequest.getRequestId(), mandatoryRequest.getServiceId(),
            mandatoryRequest.getUsername(), order.getUserId())
            .onItem().apply(com.ehy.common.helper.CommonHelper::handleRestError)
            .onItem().produceUni(user -> {
              String[] name = user.getName().split(" ");
              String firstName = name[0];
              String lastName = name[name.length - 1];

              for (int i = 1; i < name.length - 1; i++) {
                firstName = firstName.concat(" ").concat(name[i]);
              }

              ChargeRequest chargeRequest = ChargeRequest.builder()
                  .transactionDetail(TransactionDetail.builder().orderId(order.getId()).grossAmount(
                      order.getInventoryDetails().stream()
                          .mapToDouble(inv -> inv.getPrice() * inv.getQuantity()).sum()).build())
                  .itemDetails(order.getInventoryDetails().stream().map(inv -> ItemDetail.builder()
                      .id(inv.getId())
                      .name(inv.getAirlineCode())
                      .price(inv.getPrice())
                      .quantity(inv.getQuantity())
                      .build()).collect(Collectors.toList()))
                  .customerDetail(CustomerDetail.builder()
                      .firstName(firstName)
                      .lastName(lastName)
                      .email(user.getContact().getEmail())
                      .phone(user.getContact().getPhoneNumber())
                      .shippingAddress(customerMapper.toAddress(order.getCustomers().get(0)))
                      .build())
                  .build();

              Transaction transaction = Transaction.builder()
                  .orderId(order.getId())
                  .status(PaymentStatus.PENDING)
                  .request(chargeRequest)
                  .build();

              return upsert(mandatoryRequest, transaction, Action.CREATE).onItem()
                  .apply(t -> order.getId());
            })).onFailure().invoke(t -> log.error("Error create payment", t));
  }

  @Override
  public Uni<String> accept(MandatoryRequest mandatoryRequest, String id, String paymentMethodId,
      CardInfo cardInfo, Customer billingAddress) {
    return Uni.createFrom().deferred(() -> paymentMethodService.findById(paymentMethodId)
        .onItem().produceUni(paymentMethod -> {
          if (PaymentMethodType.CREDIT_CARD.equals(paymentMethod.getPaymentMethodType()) && (Objects
              .isNull(cardInfo) || Objects.isNull(billingAddress))) {
            return Uni.createFrom()
                .failure(new BusinessLogicException(ResponseCode.BIND_ERROR.getCode(),
                    "Required field is missing", Collections.singletonList(
                    "field cardInfo and billingAddress is required for credit card transactions")));
          }

          return repository()
              .findByCustomFields(Arrays.asList("orderId", "status"), id, PaymentStatus.PENDING)
              .onItem().ifNull().failWith(new BusinessLogicException(ResponseCode.DATA_NOT_EXIST,
                  Collections.singletonList("Data is not exist or might be in process")))
              .onItem().produceUni(transaction -> {
                transaction.getRequest()
                    .setPaymentType(paymentMethod.getPaymentMethodType().getValue());

                transaction.setBillingAddress(billingAddress);
                transaction.setCardInfo(cardInfo);
                transaction.setPaymentMethodName(paymentMethod.getName());
                transaction.setPaymentMethodType(paymentMethod.getPaymentMethodType());

                return acceptPayment(mandatoryRequest, transaction);
              });
        }));
  }

  private Uni<String> acceptPayment(MandatoryRequest mandatoryRequest, Transaction transaction) {
    return Uni.createFrom().deferred(() -> {
      Uni<ChargeResponse> restCall = Uni.createFrom().nothing();
      ChargeRequest chargeRequest = transaction.getRequest();
      switch (transaction.getPaymentMethodType()) {
        case BANK_TRANSFER:
          chargeRequest.setBankTransfer(
              BankTransfer.builder().bank(transaction.getPaymentMethodName())
                  .build());
          restCall = midtransService.charge(
              CommonHelper.buildBasicAuthHeader(midtransProperties.getServerKey(), null),
              chargeRequest);
          break;
        case CREDIT_CARD:
          chargeRequest.getCustomerDetail()
              .setBillingAddress(customerMapper.toAddress(transaction.getBillingAddress()));
          CardInfo cardInfo = transaction.getCardInfo();
          restCall = midtransService
              .retrieveToken(midtransProperties.getClientKey(), cardInfo.getCardNumber(),
                  cardInfo.getExpireMonth(), cardInfo.getExpireYear(), cardInfo.getCvv())
              .onItem().produceUni(tokenResponse -> {
                chargeRequest.setCreditCard(CreditCard.builder().authentication(true)
                        .tokenId(tokenResponse.getTokenId()).build());
                return midtransService.charge(CommonHelper
                        .buildBasicAuthHeader(midtransProperties.getServerKey(), null),
                    chargeRequest);
              });
          break;
        default:
          break;
      }

      return restCall.onItem()
          .produceUni(chargeResponse -> {
            if (!chargeResponse.getStatusCode().startsWith("2")) {
              return Uni.createFrom()
                  .failure(new BusinessLogicException(ResponseCode.BIND_ERROR.getCode(),
                      chargeResponse.getStatusMessage(), chargeResponse.getValidationMessages()));
            }
            transaction.setResponse(chargeResponse);
            transaction.setMidtransTransactionId(chargeResponse.getTransactionId());
            return this.upsert(mandatoryRequest, transaction, Action.CREATE)
                .onItem().apply(t -> PaymentMethodType.CREDIT_CARD.getValue()
                    .equalsIgnoreCase(chargeResponse.getPaymentType()) ? chargeResponse
                    .getRedirectUrl() : Optional.ofNullable(chargeResponse.getPermataVaNumber())
                    .orElse(chargeResponse.getVaNumbers().get(0).getVaNumber()));
          });
    }).onFailure().invoke(t -> log.error("Error confirm payment", t));
  }

  @Override
  public Uni<String> confirmPayment(ChargeResponse confirmRequest) {
    return Uni.createFrom().deferred(() -> repository()
        .findByCustomFields(Collections.singletonList("orderId"), confirmRequest.getOrderId())
        .onItem().produceUni(transaction -> {
          MandatoryRequest mandatoryRequest = MandatoryRequest.builder()
              .clientIpAddress("midtrans")
              .clientSessionId("midtrans")
              .requestId(com.ehy.common.helper.CommonHelper.generateUUID())
              .serviceId(ServiceId.PAYMENT_SERVICE)
              .username("midtrans").build();
          Uni<BaseResponse<Boolean>> uni = Uni.createFrom()
              .item(BaseResponse.<Boolean>parentBuilder().code(ResponseCode.SUCCESS.getCode())
                  .data(true).build());
          if (Arrays.asList("settlement", "capture")
              .contains(confirmRequest.getTransactionStatus()) && "accept"
              .equalsIgnoreCase(confirmRequest.getFraudStatus())) {
            transaction.setStatus(PaymentStatus.CONFIRMED);
          } else if ("deny".equals(confirmRequest.getTransactionStatus())) {
            transaction.setStatus(PaymentStatus.DENIED);
          } else {
            transaction.setStatus(PaymentStatus.PENDING);
          }

          return uni.onItem().apply(com.ehy.common.helper.CommonHelper::handleRestError).onItem()
              .produceUni(b -> upsert(mandatoryRequest, transaction,
                  Action.UPDATE).onItem().apply(t -> transaction.getStatus().name()));
        }));
  }

  @Override
  public Uni<Boolean> cancel(MandatoryRequest mandatoryRequest, String orderId) {
    return Uni.createFrom().deferred(
        () -> repository().findByCustomFields(Collections.singletonList("orderId"), orderId)
            .onItem().produceUni(transaction -> {
              transaction.setStatus(PaymentStatus.CANCELLED);
              return upsert(mandatoryRequest, transaction, Action.UPDATE);
            }));
  }

  @Override
  public Uni<Boolean> refund(MandatoryRequest mandatoryRequest, String orderId) {
    return Uni.createFrom().deferred(
        () -> repository().findByCustomFields(Arrays.asList("orderId", "status"), orderId, PaymentStatus.CONFIRMED)
            .onItem().ifNull().failWith(new BusinessLogicException(ResponseCode.BIND_ERROR.getCode(),
                "Payment is not confirmed", Collections.singletonList("Payment is not confirmed")))
            .onItem().produceUni(transaction -> {

              if (!PaymentMethodType.CREDIT_CARD.equals(transaction.getPaymentMethodType())) {
                return Uni.createFrom()
                    .failure(new BusinessLogicException(ResponseCode.BIND_ERROR.getCode(),
                        "Payment method type not supported",
                        Collections.singletonList(
                            "Only credit card payment can be directly refunded, otherwise please contact our team")));
              }

              return orderService.validateRefund(mandatoryRequest.getCurrency(),
                  mandatoryRequest.getClientIpAddress(),
                  mandatoryRequest.getClientSessionId(),
                  mandatoryRequest.getRequestId(), mandatoryRequest.getServiceId(),
                  mandatoryRequest.getUsername(), orderId)
                  .onItem().apply(com.ehy.common.helper.CommonHelper::handleRestError)
                  .onItem().produceUni(resp -> midtransService.refund(CommonHelper
                          .buildBasicAuthHeader(midtransProperties.getServerKey(), null),
                      orderId, RefundRequest.builder()
                          .refundKey(transaction.getId().toString())
                          .amount(transaction.getRequest().getTransactionDetail().getGrossAmount())
                          .reason("Customer request").build())
                      .onItem().produceUni(response -> {
                        if (!"200".equals(response.getStatusCode())) {
                          transaction.setStatus(PaymentStatus.REFUND_DENIED);
                          return upsert(mandatoryRequest, transaction, Action.UPDATE).onItem()
                              .failWith(t -> new BusinessLogicException(
                                  ResponseCode.THIRD_PARTY_ERROR.getCode(),
                                  response.getStatusMessage(),
                                  response.getValidationMessages()));
                        }
                        transaction.setStatus(PaymentStatus.REFUNDED);
                        transaction.setResponse(response);
                        return upsert(mandatoryRequest, transaction, Action.UPDATE);
                      }));
            }));
  }
}

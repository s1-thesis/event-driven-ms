package com.ehy.user.dto;

import com.ehy.common.model.BaseDto;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
public class UserDto extends BaseDto<String> {

  @NotBlank
  private String name;
  @NotBlank @Size(min = 6, max = 15)
  private String username;
  @NotBlank
  private String password;
  @NotNull
  private Contact contact;
  private String role;

  @Builder
  public UserDto(String id, @NotBlank String name,
      @NotBlank @Size(min = 6, max = 15) String username,
      @NotBlank String password,
      @NotNull Contact contact, String role) {
    super(id);
    this.name = name;
    this.username = username;
    this.password = password;
    this.contact = contact;
    this.role = role;
  }
}

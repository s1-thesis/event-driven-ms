package com.ehy.user.application.dao.impl;

import com.ehy.user.application.dao.interfaces.UserRepository;
import com.ehy.user.application.entity.domain.User;
import javax.enterprise.context.ApplicationScoped;

@ApplicationScoped
public class UserRepositoryImpl extends BaseRepositoryImpl<User> implements UserRepository {

}

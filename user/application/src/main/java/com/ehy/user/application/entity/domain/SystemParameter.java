package com.ehy.user.application.entity.domain;

import com.ehy.user.application.entity.constant.CollectionName;
import io.quarkus.mongodb.panache.MongoEntity;
import java.time.Instant;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;
import org.bson.types.ObjectId;

@Data
@NoArgsConstructor
@EqualsAndHashCode(callSuper = true)
@ToString(callSuper = true)
@MongoEntity(collection = CollectionName.SYSTEM_PARAMETER)
public class SystemParameter extends BaseMongo {

  private String variable;
  private String value;
  private String description;

  @Builder
  public SystemParameter(ObjectId id, String createdBy, String updatedBy,
      Instant createdAt, Instant updatedAt, boolean deleted, String variable, String value,
      String description) {
    super(id, createdBy, updatedBy, createdAt, updatedAt, deleted);
    this.variable = variable;
    this.value = value;
    this.description = description;
  }
}

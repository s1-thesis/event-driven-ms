package com.ehy.user.application.service.impl;

import com.ehy.common.constant.ResponseCode;
import com.ehy.common.exception.BusinessLogicException;
import com.ehy.common.model.MandatoryRequest;
import com.ehy.user.application.dao.interfaces.BaseRepository;
import com.ehy.user.application.dao.interfaces.UserRepository;
import com.ehy.user.application.entity.constant.BaseMongoFields;
import com.ehy.user.application.entity.domain.Role;
import com.ehy.user.application.entity.domain.User;
import com.ehy.user.application.libraries.helper.PasswordHelper;
import com.ehy.user.application.service.interfaces.UserService;
import io.smallrye.mutiny.Uni;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Objects;
import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import lombok.extern.slf4j.Slf4j;
import org.springframework.util.CollectionUtils;

@Slf4j
@ApplicationScoped
public class UserServiceImpl extends BaseResourceServiceImpl<User> implements UserService {

  @Inject
  UserRepository userRepository;

  @Override
  protected BaseRepository<User> repository() {
    return userRepository;
  }

  // TODO
  @Override
  protected User mapUpdate(User oldData, User newData) {
    return null;
  }

  @Override
  public Uni<User> findByUsername(String username) {
    return repository()
        .findByCustomFields(Arrays.asList("username", BaseMongoFields.DELETED), username, false);
  }

  @Override
  public Uni<Boolean> create(MandatoryRequest mandatoryRequest, User user) {
    List<String> passwordErrors = PasswordHelper.isPasswordValid(user.getPassword());

    if (!CollectionUtils.isEmpty(passwordErrors)) {
      return Uni.createFrom().failure(
          new BusinessLogicException(ResponseCode.BIND_ERROR.getCode(), "Password is not valid",
              passwordErrors));
    }

    return findByUsername(user.getUsername())
        .onItem().apply(t -> {
          if (Objects.nonNull(t)) {
            throw new BusinessLogicException(ResponseCode.DUPLICATE_DATA,
                Collections.singletonList(ResponseCode.DUPLICATE_DATA.getMessage()));
          }

          return user;
        }).onItem().produceUni(u -> {
          u.setRole(Role.USER);
          u.setPassword(PasswordHelper.encryptPassword(user.getPassword()));
          return super.create(mandatoryRequest, user);
        });
  }
}

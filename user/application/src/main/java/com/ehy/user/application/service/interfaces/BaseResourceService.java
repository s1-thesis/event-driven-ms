package com.ehy.user.application.service.interfaces;

import com.ehy.common.model.MandatoryRequest;
import com.ehy.common.model.PageWrapper;
import com.ehy.user.application.entity.domain.BaseMongo;
import io.smallrye.mutiny.Uni;

public interface BaseResourceService<T extends BaseMongo> {

  Uni<PageWrapper<T>> findAll(Integer page, Integer size);

  Uni<T> findById(String id);

  Uni<Boolean> create(MandatoryRequest mandatoryRequest, T systemParameter);

  Uni<Boolean> update(MandatoryRequest mandatoryRequest, T systemParameter);

  Uni<Boolean> delete(MandatoryRequest mandatoryRequest, String id);
}

package com.ehy.user.application.service.interfaces;

import com.ehy.user.application.entity.domain.SystemParameter;

public interface SystemParameterService extends BaseResourceService<SystemParameter> {

}

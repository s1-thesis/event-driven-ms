package com.ehy.user.application.dao.interfaces;

import com.ehy.user.application.entity.domain.SystemParameter;

public interface SystemParameterRepository extends BaseRepository<SystemParameter> {

}

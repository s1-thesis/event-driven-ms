package com.ehy.user.application.entity.enums;

public enum Action {
  CREATE, UPDATE, DELETE
}

package com.ehy.user.application.web.controller;

import com.ehy.common.model.BaseResponse;
import com.ehy.common.model.MandatoryRequest;
import com.ehy.user.application.entity.constant.ApiPath;
import com.ehy.user.application.service.interfaces.AuthService;
import com.ehy.user.dto.AuthDto;
import io.smallrye.mutiny.Uni;
import javax.inject.Inject;
import javax.validation.Valid;
import javax.ws.rs.BeanParam;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import org.eclipse.microprofile.openapi.annotations.tags.Tag;
import org.eclipse.microprofile.openapi.annotations.tags.Tags;

@Path(ApiPath.BASE_PATH_AUTH)
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
@Tags(value = @Tag(name = "Auth", description = "Auth resource"))
public class AuthController extends BaseResource {

  @Inject
  AuthService authService;

  @POST
  @Path(ApiPath.APPEND_TOKEN)
  public Uni<BaseResponse<String>> retrieveToken(
      @BeanParam @Valid MandatoryRequest mandatoryRequest,
      @Valid AuthDto authDto) {
    return buildResponse(
        authService.retrieveToken(mandatoryRequest, authDto.getUsername(), authDto.getPassword()));
  }

  @POST
  @Path(ApiPath.APPEND_REGISTER)
  public Uni<BaseResponse> register(
      @BeanParam @Valid MandatoryRequest mandatoryRequest) {
    return null;
  }
}

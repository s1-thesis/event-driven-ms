package com.ehy.user.application.dao.impl;

import com.ehy.user.application.dao.interfaces.SystemParameterRepository;
import com.ehy.user.application.entity.domain.SystemParameter;
import javax.enterprise.context.ApplicationScoped;

@ApplicationScoped
public class SystemParameterRepositoryImpl extends BaseRepositoryImpl<SystemParameter> implements SystemParameterRepository {

}

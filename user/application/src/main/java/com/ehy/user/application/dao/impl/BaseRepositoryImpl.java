package com.ehy.user.application.dao.impl;

import com.ehy.common.model.MandatoryRequest;
import com.ehy.user.application.dao.interfaces.BaseRepository;
import com.ehy.user.application.entity.constant.BaseMongoFields;
import com.ehy.user.application.entity.domain.BaseMongo;
import com.ehy.user.application.entity.enums.Action;
import com.ehy.user.application.libraries.helper.CommonHelper;
import io.quarkus.mongodb.panache.reactive.ReactivePanacheQuery;
import io.quarkus.panache.common.Page;
import io.quarkus.panache.common.Sort;
import io.smallrye.mutiny.Multi;
import io.smallrye.mutiny.Uni;
import java.util.List;
import org.bson.types.ObjectId;

public class BaseRepositoryImpl<T extends BaseMongo> implements BaseRepository<T> {

  @Override
  public Multi<T> findAllByDeleted(boolean deleted) {
    return this.find(BaseMongoFields.DELETED + " = ?1", deleted, Sort.ascending(BaseMongoFields.ID))
        .stream();
  }

  @Override
  public ReactivePanacheQuery<T> findAllByDeleted(boolean deleted, Page page) {
    return this.find(BaseMongoFields.DELETED + " = ?1", deleted, Sort.ascending(BaseMongoFields.ID))
        .page(page);
  }

  @Override
  public Uni<T> findByIdAndDeleted(String id, boolean deleted) {
    return this.find(BaseMongoFields.ID + " = ?1 and " + BaseMongoFields.DELETED + " = ?2",
        new ObjectId(id), deleted).firstResult();
  }

  @Override
  public Uni<Long> countByDeleted(boolean deleted) {
    return this.count(BaseMongoFields.DELETED + " = ?1", deleted);
  }

  @Override
  public Uni<Void> upsert(MandatoryRequest mandatoryRequest, T t, Action action) {
    return this.persistOrUpdate(CommonHelper.setBaseMongoFields(mandatoryRequest, t, action));
  }

  @Override
  public Uni<T> findByCustomFields(List<String> fields, Object... o) {
    return this.find(buildQuery(fields, o).toString(), o).firstResult();
  }

  public StringBuilder buildQuery(List<String> fields, Object... o) {
    StringBuilder sb = new StringBuilder();

    for (int i = 0; i < fields.size(); i++) {
      sb.append(fields.get(i)).append(" =?").append(i + 1);
      if (i != fields.size() - 1) {
        sb.append(" and ");
      }
    }

    return sb;
  }
}

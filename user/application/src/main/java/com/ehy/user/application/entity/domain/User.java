package com.ehy.user.application.entity.domain;

import com.ehy.user.application.entity.constant.CollectionName;
import io.quarkus.mongodb.panache.MongoEntity;
import java.time.Instant;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;
import org.bson.types.ObjectId;

@Data
@NoArgsConstructor
@EqualsAndHashCode(callSuper = true)
@ToString(callSuper = true)
@MongoEntity(collection = CollectionName.USER)
public class User extends BaseMongo {

  private String name;
  private String username;
  private String password;
  private Contact contact;
  private Role role;

  @Builder
  public User(ObjectId id, String createdBy, String updatedBy,
      Instant createdAt, Instant updatedAt, boolean deleted,
      String name, String username, String password,
      Contact contact, Role role) {
    super(id, createdBy, updatedBy, createdAt, updatedAt, deleted);
    this.name = name;
    this.username = username;
    this.password = password;
    this.contact = contact;
    this.role = role;
  }
}

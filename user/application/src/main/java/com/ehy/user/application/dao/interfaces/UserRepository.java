package com.ehy.user.application.dao.interfaces;

import com.ehy.user.application.entity.domain.User;

public interface UserRepository extends BaseRepository<User> {

}

package com.ehy.user.application.service.interfaces;

import com.ehy.user.application.entity.domain.User;
import io.smallrye.mutiny.Uni;

public interface UserService extends BaseResourceService<User> {

  Uni<User> findByUsername(String username);
}

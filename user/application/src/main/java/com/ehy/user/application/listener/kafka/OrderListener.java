package com.ehy.user.application.listener.kafka;

import com.ehy.common.constant.ConstantValues;
import com.ehy.common.enums.OrderStatus;
import com.ehy.message.dto.EmailDto;
import com.ehy.order.dto.OrderEvent;
import com.ehy.user.application.entity.domain.User;
import com.ehy.user.application.service.interfaces.UserService;
import io.smallrye.mutiny.Uni;
import io.smallrye.mutiny.tuples.Tuple2;
import java.time.Duration;
import java.time.temporal.ChronoUnit;
import java.util.concurrent.CompletionStage;
import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import lombok.extern.slf4j.Slf4j;
import org.eclipse.microprofile.reactive.messaging.Channel;
import org.eclipse.microprofile.reactive.messaging.Emitter;
import org.eclipse.microprofile.reactive.messaging.Incoming;

@ApplicationScoped
@Slf4j
public class OrderListener {

  @Inject
  @Channel("email")
  Emitter<EmailDto> emailEmitter;

  @Inject
  UserService userService;

  @Incoming("order")
  public CompletionStage<Void> handleOrderEvent(OrderEvent order) {
    log.info("[KAFKA] Receive order event -> {}", order);

    if (!OrderStatus.ACTIVE.equals(order.getFullDocument().getStatus())) {
      return Uni.createFrom().<Void>nothing().subscribeAsCompletionStage();
    }

    return userService
        .findById(order.getFullDocument().getOrderDetail().getUserId())
        .onItem()
        .apply(user -> {
          Tuple2<String, String> tuple = retrieveSubjectAndText(user, order);

          emailEmitter.send(EmailDto.builder()
              .from(ConstantValues.NO_REPLY_EMAIL)
              .to(user.getContact().getEmail())
              .subject(tuple.getItem1())
              .text(tuple.getItem2())
              .username(ConstantValues.SYSTEM)
              .build());

          return user;
        }).onItem().delayIt().by(Duration.of(100, ChronoUnit.MILLIS))
        .onFailure().invoke(t -> log
            .error("Error processing order -> order id {}", order.getFullDocument().getId(), t))
        .subscribeAsCompletionStage()
        .thenAccept(t -> log
            .info("Successfully processing order for {}", order.getFullDocument().getId()));
  }

  private Tuple2<String, String> retrieveSubjectAndText(User user, OrderEvent order) {
    String subject = "";
    String text = "";
    com.ehy.order.dto.domain.Order orderDocument = order.getFullDocument();
    if (OrderStatus.ACTIVE.equals(orderDocument.getStatus())) {
      subject = "[EHY] Your order has been placed";
      text = "Hi " + user.getName()
          + "\n\nThank you for your order.\nIn several minutes an e-mail will be sent to complete the payment.\n\nRegards,\nEHY";
    } else if (OrderStatus.CONFIRMED.equals(orderDocument.getStatus())) {
      subject = "[EHY] Your order has been confirmed";
      text = "Hi " + user.getName()
          + "\n\nYour order with id " + orderDocument.getId()
          + " has been confirmed.\n In several minutes payment receipt will be sent to your email.\n\nRegards,\nEHY";
    } else if (OrderStatus.CANCELLED.equals(orderDocument.getStatus())) {
      subject = "[EHY] Your order has been cancelled";
      text = "Hi " + user.getName()
          + "\n\nYour order with id " + orderDocument.getId()
          + " has been cancelled.\n\nRegards,\nEHY";
    } else if (OrderStatus.FAILED.equals(orderDocument.getStatus())) {
      subject = "[EHY] Your order has been rejected";
      text = "Hi " + user.getName()
          + "\n\nYour order with id " + orderDocument.getId()
          + " has been rejected.\nPlease proceed with a new order and payment transaction.\n\nRegards,\nEHY";
    }

    return Tuple2.of(subject, text);
  }
}

package com.ehy.user.application.entity.domain;

import lombok.Getter;

@Getter
public enum  Role {

  ADMIN, USER
}

package com.ehy.user.application.web.controller;

import com.ehy.common.model.BaseResponse;
import com.ehy.common.model.MandatoryRequest;
import com.ehy.user.application.entity.constant.ApiPath;
import com.ehy.user.application.libraries.mapper.UserMapper;
import com.ehy.user.application.service.interfaces.UserService;
import com.ehy.user.dto.UserDto;
import io.smallrye.mutiny.Uni;
import javax.inject.Inject;
import javax.validation.Valid;
import javax.ws.rs.BeanParam;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import org.eclipse.microprofile.openapi.annotations.tags.Tag;
import org.eclipse.microprofile.openapi.annotations.tags.Tags;
import org.jboss.resteasy.annotations.jaxrs.PathParam;

@Path(ApiPath.BASE_PATH_USERS)
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
@Tags(value = @Tag(name = "User", description = "User resource"))
public class UserController extends BaseResource {

  @Inject
  UserService userService;

  @Inject
  UserMapper userMapper;

  @GET
  @Path(ApiPath.APPEND_ID)
  public Uni<BaseResponse<UserDto>> findOne(
      @BeanParam @Valid MandatoryRequest mandatoryRequest,
      @PathParam String id) {
    return buildResponse(userService.findById(id).onItem().apply(user -> userMapper.toDto(user)));
  }

  @POST
  public Uni<BaseResponse<Boolean>> create(
      @BeanParam @Valid MandatoryRequest mandatoryRequest,
      @Valid UserDto userDto) {
    return buildResponse(userService.create(mandatoryRequest, userMapper.toDomain(userDto)));
  }
}

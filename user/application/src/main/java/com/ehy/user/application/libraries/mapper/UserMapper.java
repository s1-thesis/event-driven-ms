package com.ehy.user.application.libraries.mapper;

import com.ehy.user.application.entity.domain.User;
import com.ehy.user.dto.UserDto;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(componentModel = "cdi")
public interface UserMapper extends BaseMapper {

  @Mapping(target = "id", source = "user.id", qualifiedByName = "objectIdToString")
  UserDto toDto(User user);

  @Mapping(target = "id", source = "userDto.id", qualifiedByName = "stringToObjectId")
  User toDomain(UserDto userDto);
}

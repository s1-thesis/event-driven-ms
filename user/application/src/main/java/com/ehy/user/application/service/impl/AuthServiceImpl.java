package com.ehy.user.application.service.impl;

import com.auth0.jwt.JWT;
import com.auth0.jwt.algorithms.Algorithm;
import com.ehy.common.constant.ResponseCode;
import com.ehy.common.exception.BusinessLogicException;
import com.ehy.common.model.MandatoryRequest;
import com.ehy.user.application.libraries.helper.PasswordHelper;
import com.ehy.user.application.service.interfaces.AuthService;
import com.ehy.user.application.service.interfaces.UserService;
import io.smallrye.mutiny.Uni;
import java.util.Collections;
import java.util.Date;
import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import lombok.extern.slf4j.Slf4j;
import org.eclipse.microprofile.config.inject.ConfigProperty;

@Slf4j
@ApplicationScoped
public class AuthServiceImpl implements AuthService {

  @Inject
  UserService userService;

  @ConfigProperty(name = "jwt.secret")
  private String secret;

  @Override
  public Uni<String> retrieveToken(MandatoryRequest mandatoryRequest, String username,
      String password) {
    return Uni.createFrom().deferred(() -> userService.findByUsername(username)
        .onItem().ifNull().failWith(new BusinessLogicException(ResponseCode.DATA_NOT_EXIST,
            Collections.singletonList(ResponseCode.DATA_NOT_EXIST.getMessage())))
        .onItem().produceUni(user -> {
          if (!PasswordHelper.matchPassword(password, user.getPassword())) {
            return Uni.createFrom().failure(new BusinessLogicException(ResponseCode.BIND_ERROR,
                Collections.singletonList("Invalid credentials provided")));
          }

          Date currentDate = new Date();
          try {
            Algorithm algorithm = Algorithm.HMAC256(secret);
            return Uni.createFrom().item(JWT.create()
                .withClaim("userId", user.getId().toString())
                .withClaim("createdAt", currentDate)
                .withExpiresAt(new Date(currentDate.getTime() + 86400000))
                .sign(algorithm));
          } catch (Exception e) {
            return Uni.createFrom().failure(new BusinessLogicException(ResponseCode.SYSTEM_ERROR,
                Collections.singletonList("Error generating token")));
          }
        }));
  }
}

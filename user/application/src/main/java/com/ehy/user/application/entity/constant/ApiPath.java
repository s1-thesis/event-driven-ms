package com.ehy.user.application.entity.constant;

public class ApiPath {

  /**
   * Rename this with service name
   */
  public static final String BASE_PATH = "/user-service";

  public static final String APPEND_SYSTEM_PARAMETERS = "/system-parameters";
  public static final String APPEND_ID = "/{id}";
  public static final String APPEND_USERS = "/users";
  public static final String APPEND_AUTH = "/auth";
  public static final String APPEND_TOKEN = "/token";
  public static final String APPEND_REGISTER = "/register";

  /**
   * Base path
   */
  public static final String BASE_PATH_SYSTEM_PARAMETERS = BASE_PATH + APPEND_SYSTEM_PARAMETERS;
  public static final String BASE_PATH_USERS = BASE_PATH + APPEND_USERS;
  public static final String BASE_PATH_AUTH = BASE_PATH + APPEND_AUTH;
}

package com.ehy.user.application.service.interfaces;

import com.ehy.common.model.MandatoryRequest;
import io.smallrye.mutiny.Uni;

public interface AuthService {

  Uni<String> retrieveToken(MandatoryRequest mandatoryRequest, String username, String password);
}

package com.ehy.user.application.libraries.helper;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import org.passay.DigitCharacterRule;
import org.passay.LengthRule;
import org.passay.PasswordData;
import org.passay.PasswordValidator;
import org.passay.RuleResult;
import org.passay.RuleResultDetail;
import org.passay.SpecialCharacterRule;
import org.passay.UppercaseCharacterRule;
import org.passay.WhitespaceRule;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

public class PasswordHelper {

  private PasswordHelper() {
  }

  public static String encryptPassword(String rawPassword) {
    BCryptPasswordEncoder bCryptPasswordEncoder = new BCryptPasswordEncoder();
    return bCryptPasswordEncoder.encode(rawPassword);
  }

  public static boolean matchPassword(String rawPassword, String password) {
    BCryptPasswordEncoder bCryptPasswordEncoder = new BCryptPasswordEncoder();
    return bCryptPasswordEncoder.matches(rawPassword, password);
  }

  public static List<String> isPasswordValid(String password) {
    PasswordValidator validator = new PasswordValidator(Arrays.asList(
        new LengthRule(8, 30),
        new UppercaseCharacterRule(1),
        new DigitCharacterRule(1),
        new SpecialCharacterRule(1),
        new WhitespaceRule()));

    RuleResult result = validator.validate(new PasswordData(password));
    if (!result.isValid()) {
      Set<String> errorList = new HashSet<>();
      for (RuleResultDetail ruleResultDetail : result.getDetails()) {
        errorList.add(ruleResultDetail.getErrorCode());
      }
      return new ArrayList<>(errorList);
    }
    return Collections.emptyList();
  }
}

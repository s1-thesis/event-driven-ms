#!/usr/bin/env sh
set -eux

apk add curl

. config.sh

registry='registry.gitlab.com/s1-thesis/event-driven-ms'

if [ -n "${CI_REGISTRY_USER+x}" ]; then
  docker login -u "${CI_REGISTRY_USER}" -p "$CI_REGISTRY_PASSWORD" "$CI_REGISTRY"
fi

for service in $services graphql-service; do
  tag="$registry/$service"
  sha_tag="$tag:$CI_COMMIT_SHA" 
  if [ "$service" = "graphql-service" ]; then
    docker build --pull -t "$tag" -f "./Dockerfile.graphql-service" "$service"
  else
    docker build --pull -t "$tag" -f "./Dockerfile" "$service/application"
  fi
  docker tag "$tag" "$sha_tag"
  docker push "$tag"
  docker push "$sha_tag"
  curl \
    -d '{"project": "'"$service"'", "environment": "", "tag": "'"$CI_COMMIT_SHA"'"}' \
    'https://deployer.s1-thesis.xyz/deploy?token=Vi8equiuvohg9aGifahsiyicuqueimee'
done

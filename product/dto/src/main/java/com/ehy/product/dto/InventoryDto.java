package com.ehy.product.dto;

import com.ehy.common.model.BaseDto;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Data
@EqualsAndHashCode(callSuper = true)
@NoArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
public class InventoryDto extends BaseDto<String> {

  @NotBlank
  private String airlineCode;
  @NotBlank
  private String date;
  @NotBlank
  private String departureTime;
  @NotBlank
  private String arrivalTime;
  @NotBlank
  private String departureLocation;
  @NotBlank
  private String arrivalLocation;
  @NotNull @Min(0) @Max(300)
  private Integer allotment;
  @NotNull
  private PricingDto pricing;
  @NotBlank
  private String cancellationPolicyId;
  private CancellationPolicyDto cancellationPolicy;
  @JsonIgnore
  private String updatedBy;

  @Builder
  public InventoryDto(String id, @NotBlank String airlineCode,
      @NotBlank String date, @NotBlank String departureTime,
      @NotBlank String arrivalTime,
      @NotBlank String departureLocation,
      @NotBlank String arrivalLocation,
      @NotNull @Min(0) @Max(300) Integer allotment,
      @NotNull PricingDto pricing,
      @NotBlank String cancellationPolicyId, CancellationPolicyDto cancellationPolicy) {
    super(id);
    this.airlineCode = airlineCode;
    this.date = date;
    this.departureTime = departureTime;
    this.arrivalTime = arrivalTime;
    this.departureLocation = departureLocation;
    this.arrivalLocation = arrivalLocation;
    this.allotment = allotment;
    this.pricing = pricing;
    this.cancellationPolicyId = cancellationPolicyId;
    this.cancellationPolicy = cancellationPolicy;
  }
}

package com.ehy.product.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Builder;
import lombok.Data;

@Data
@Builder
@JsonIgnoreProperties(ignoreUnknown = true)
public class SearchDto {
    private Integer page;
    private Integer size;
    private String departure;
    private String arrival;
    private String date;
    private Integer passenger;
}

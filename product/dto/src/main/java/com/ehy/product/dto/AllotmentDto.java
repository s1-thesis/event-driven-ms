package com.ehy.product.dto;

import com.ehy.common.model.BaseDto;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Data
@EqualsAndHashCode(callSuper = true)
@NoArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
public class AllotmentDto extends BaseDto<String> {

  @NotNull @Min(1)
  private Integer allotment;

  @NotNull
  private AllotmentType type;

  @Builder
  public AllotmentDto(String id,
      @NotNull @Min(1) Integer allotment,
      @NotNull AllotmentType type) {
    super(id);
    this.allotment = allotment;
    this.type = type;
  }
}

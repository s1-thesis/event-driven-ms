package com.ehy.product.dto;

import com.ehy.common.model.BaseKafkaSource;
import com.ehy.common.model.kafka.DocumentKey;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Data
@NoArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
@EqualsAndHashCode(callSuper = true)
@ToString(callSuper = true)
public class InventoryEvent extends BaseKafkaSource<InventoryDto> {

  @Builder
  public InventoryEvent(String operationType, InventoryDto fullDocument,
      DocumentKey documentKey) {
    super(operationType, fullDocument, documentKey);
  }
}

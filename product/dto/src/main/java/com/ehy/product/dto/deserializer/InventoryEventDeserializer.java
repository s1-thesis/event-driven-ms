package com.ehy.product.dto.deserializer;

import com.ehy.product.dto.InventoryEvent;
import io.quarkus.kafka.client.serialization.ObjectMapperDeserializer;

public class InventoryEventDeserializer extends ObjectMapperDeserializer<InventoryEvent> {

  public InventoryEventDeserializer() {
    super(InventoryEvent.class);
  }
}

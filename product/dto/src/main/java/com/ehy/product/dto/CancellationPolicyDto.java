package com.ehy.product.dto;

import com.ehy.common.model.BaseDto;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import javax.validation.constraints.DecimalMax;
import javax.validation.constraints.DecimalMin;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Data
@EqualsAndHashCode(callSuper = true)
@NoArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
public class CancellationPolicyDto extends BaseDto<String> {

  private boolean refundable;
  @NotNull @DecimalMin("00.00") @DecimalMax("100.00")
  private Double percentage;
  @NotNull @Min(0) @Max(365)
  private Integer daysBefore;

  @Builder
  public CancellationPolicyDto(String id, boolean refundable,
      @NotNull @DecimalMin("00.00") @DecimalMax("100.00") Double percentage,
      @NotNull @Min(0) @Max(365) Integer daysBefore) {
    super(id);
    this.refundable = refundable;
    this.percentage = percentage;
    this.daysBefore = daysBefore;
  }
}

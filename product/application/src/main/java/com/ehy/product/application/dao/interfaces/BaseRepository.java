package com.ehy.product.application.dao.interfaces;

import com.ehy.common.model.MandatoryRequest;
import com.ehy.product.application.entity.enums.Action;
import com.ehy.product.application.entity.enums.MongoComparator;
import io.quarkus.mongodb.panache.reactive.ReactivePanacheMongoRepository;
import io.quarkus.mongodb.panache.reactive.ReactivePanacheQuery;
import io.quarkus.panache.common.Page;
import io.smallrye.mutiny.Multi;
import io.smallrye.mutiny.Uni;
import io.smallrye.mutiny.tuples.Tuple3;
import java.util.List;

public interface BaseRepository<T> extends ReactivePanacheMongoRepository<T> {

  Multi<T> findAllByDeleted(boolean deleted);

  ReactivePanacheQuery<T> findAllByDeleted(boolean deleted, Page page);

  Uni<T> findByIdAndDeleted(String id, boolean deleted);

  Uni<Long> countByDeleted(boolean deleted);

  Uni<Void> upsert(MandatoryRequest mandatoryRequest, T t, Action action);

  Uni<Void> upsert(MandatoryRequest mandatoryRequest, List<T> t, Action action);

  Uni<List<T>> findAllByIdsAndDeleted(List<String> ids, boolean deleted);

  ReactivePanacheQuery<T> findByCustomFields(Page page, List<Tuple3<String, MongoComparator, Object>> fields);
}

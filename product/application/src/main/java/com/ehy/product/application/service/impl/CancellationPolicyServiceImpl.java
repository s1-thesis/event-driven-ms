package com.ehy.product.application.service.impl;

import com.ehy.product.application.dao.interfaces.BaseRepository;
import com.ehy.product.application.dao.interfaces.CancellationPolicyRepository;
import com.ehy.product.application.entity.domain.CancellationPolicy;
import com.ehy.product.application.libraries.mapper.CancellationPolicyMapper;
import com.ehy.product.application.service.interfaces.CancellationPolicyService;
import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import lombok.extern.slf4j.Slf4j;

@ApplicationScoped
@Slf4j
public class CancellationPolicyServiceImpl extends
    BaseResourceServiceImpl<CancellationPolicy> implements CancellationPolicyService {

  @Inject
  CancellationPolicyRepository cancellationPolicyRepository;

  @Inject
  CancellationPolicyMapper mapper;

  @Override
  protected BaseRepository<CancellationPolicy> repository() {
    return cancellationPolicyRepository;
  }

  @Override
  protected CancellationPolicy mapUpdate(CancellationPolicy oldData, CancellationPolicy newData) {
    return mapper.from(oldData, newData);
  }
}

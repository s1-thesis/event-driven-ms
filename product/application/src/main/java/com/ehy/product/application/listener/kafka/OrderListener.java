package com.ehy.product.application.listener.kafka;

import com.ehy.common.enums.OrderStatus;
import com.ehy.common.model.MandatoryRequest;
import com.ehy.order.dto.OrderEvent;
import com.ehy.order.dto.domain.Order;
import com.ehy.product.application.service.interfaces.InventoryService;
import com.ehy.product.dto.AllotmentDto;
import com.ehy.product.dto.AllotmentType;
import java.util.List;
import java.util.concurrent.CompletionStage;
import java.util.stream.Collectors;
import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import lombok.extern.slf4j.Slf4j;
import org.eclipse.microprofile.reactive.messaging.Incoming;

@ApplicationScoped
@Slf4j
public class OrderListener {

  @Inject
  InventoryService inventoryService;

  @Incoming("order")
  public CompletionStage<Void> handleOrderEvent(OrderEvent orderEvent) {
    log.info("[KAFKA] Receive order event -> {}", orderEvent);

    Order order = orderEvent.getFullDocument();

    AllotmentType type =
        OrderStatus.ACTIVE.equals(order.getStatus()) ? AllotmentType.DEDUCT : AllotmentType.ADD;

    List<AllotmentDto> allotmentDtoList = order.getOrderDetail().getInventoryDetails()
        .stream().map(d -> AllotmentDto.builder()
            .id(d.getId())
            .allotment(d.getQuantity())
            .type(type)
            .build()).collect(Collectors.toList());

    return inventoryService.updateAllotment(
        MandatoryRequest.builder().username(order.getUpdatedBy()).build(), allotmentDtoList)
        .onFailure().invoke(t ->
            log.error("Error updating allotment for order id = {}", order.getId(), t))
        .subscribeAsCompletionStage()
        .thenAccept(i -> log.info("Success deduct/add inventories for order id = {}", order.getId()));
  }
}

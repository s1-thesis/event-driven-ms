package com.ehy.product.application.libraries.mapper;

import com.ehy.product.application.entity.domain.Inventory;
import com.ehy.product.dto.InventoryDto;
import java.time.Instant;
import java.time.LocalDate;
import java.time.ZoneOffset;
import java.time.format.DateTimeFormatter;
import java.util.List;
import org.mapstruct.IterableMapping;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;
import org.mapstruct.Named;

@Mapper(componentModel = "cdi")
public interface InventoryMapper extends BaseMapper {

  @Named("toDto")
  @Mappings({
      @Mapping(target = "id", source = "inventory.id", qualifiedByName = "objectIdToString"),
      @Mapping(target = "date", source = "inventory.date", dateFormat = "yyyy-MM-dd"),
      @Mapping(target = "cancellationPolicy.id", source = "cancellationPolicy.id", qualifiedByName = "objectIdToString"),
      @Mapping(target = "cancellationPolicyId", source = "cancellationPolicy.id", qualifiedByName = "objectIdToString")
  })
  InventoryDto toDto(Inventory inventory);

  @Mappings({
      @Mapping(target = "date", source = "inventoryDto.date", qualifiedByName = "toInstant"),
      @Mapping(target = "id", source = "inventoryDto.id", qualifiedByName = "stringToObjectId"),
      @Mapping(target = "cancellationPolicy.id", source = "cancellationPolicy.id", qualifiedByName = "stringToObjectId")
  })
  Inventory toDomain(InventoryDto inventoryDto);

  @IterableMapping(qualifiedByName = "toDto")
  List<InventoryDto> toDtoList(List<Inventory> inventories);

  @Named("toInstant")
  default Instant toInstant(String date) {
    return LocalDate.parse(date, DateTimeFormatter.ofPattern("yyyy-MM-dd")).atStartOfDay()
        .toInstant(ZoneOffset.UTC);
  }
}

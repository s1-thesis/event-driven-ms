package com.ehy.product.application.dao.interfaces;

import com.ehy.product.application.entity.domain.CancellationPolicy;

public interface CancellationPolicyRepository extends BaseRepository<CancellationPolicy> {

}

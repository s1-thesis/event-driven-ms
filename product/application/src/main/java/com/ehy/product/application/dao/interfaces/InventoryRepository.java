package com.ehy.product.application.dao.interfaces;

import com.ehy.product.application.entity.domain.Inventory;

public interface InventoryRepository extends BaseRepository<Inventory> {

}

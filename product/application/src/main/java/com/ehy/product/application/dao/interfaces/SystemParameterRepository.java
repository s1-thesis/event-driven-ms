package com.ehy.product.application.dao.interfaces;

import com.ehy.product.application.entity.domain.SystemParameter;

public interface SystemParameterRepository extends BaseRepository<SystemParameter> {

}

package com.ehy.product.application.libraries.mapper;

import io.netty.util.internal.StringUtil;
import java.util.Objects;
import org.bson.types.ObjectId;
import org.mapstruct.Named;

public interface BaseMapper {

  @Named("objectIdToString")
  default String map(ObjectId value) {
    if (Objects.nonNull(value)) {
      return value.toString();
    }

    return null;
  }

  @Named("stringToObjectId")
  default ObjectId map(String value) {
    if (StringUtil.isNullOrEmpty(value)) {
      return null;
    }
    return new ObjectId(value);
  }
}

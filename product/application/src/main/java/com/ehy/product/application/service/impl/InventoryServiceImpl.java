package com.ehy.product.application.service.impl;

import com.ehy.common.constant.ResponseCode;
import com.ehy.common.exception.BusinessLogicException;
import com.ehy.common.model.BaseDto;
import com.ehy.common.model.MandatoryRequest;
import com.ehy.common.model.PageInfo;
import com.ehy.common.model.PageWrapper;
import com.ehy.product.application.dao.interfaces.BaseRepository;
import com.ehy.product.application.dao.interfaces.InventoryRepository;
import com.ehy.product.application.entity.domain.CancellationPolicy;
import com.ehy.product.application.entity.domain.Inventory;
import com.ehy.product.application.entity.enums.Action;
import com.ehy.product.application.entity.enums.MongoComparator;
import com.ehy.product.application.service.interfaces.CancellationPolicyService;
import com.ehy.product.application.service.interfaces.InventoryService;
import com.ehy.product.dto.AllotmentDto;
import com.ehy.product.dto.SearchDto;
import io.netty.util.internal.StringUtil;
import io.quarkus.mongodb.panache.reactive.ReactivePanacheQuery;
import io.quarkus.panache.common.Page;
import io.smallrye.mutiny.Uni;
import io.smallrye.mutiny.tuples.Tuple3;
import lombok.extern.slf4j.Slf4j;
import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import java.time.Instant;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@ApplicationScoped
@Slf4j
public class InventoryServiceImpl extends BaseResourceServiceImpl<Inventory> implements
    InventoryService {

  @Inject
  InventoryRepository inventoryRepository;

  @Inject
  CancellationPolicyService cancellationPolicyService;

  @Override
  protected BaseRepository<Inventory> repository() {
    return inventoryRepository;
  }

  @Override
  public Uni<Boolean> create(MandatoryRequest mandatoryRequest, Inventory object) {
    return Uni.createFrom()
        .deferred(() -> cancellationPolicyService.findById(object.getCancellationPolicyId())
            .onItem().ifNull()
            .failWith(new BusinessLogicException(ResponseCode.DATA_NOT_EXIST,
                Collections.singletonList("Cancellation policy is not exist")))
            .onItem().produceUni(cp -> {
              object.setCancellationPolicy(cp);
              return super.create(mandatoryRequest, object);
            }));
  }

  @Override
  public Uni<Boolean> update(MandatoryRequest mandatoryRequest, Inventory object) {
    return Uni.createFrom()
        .deferred(() -> cancellationPolicyService.findById(object.getCancellationPolicyId())
            .onItem().ifNull()
            .failWith(new BusinessLogicException(ResponseCode.DATA_NOT_EXIST,
                Collections.singletonList("Cancellation policy is not exist")))
            .onItem().produceUni(cp -> {
              object.setCancellationPolicy(cp);
              return super.update(mandatoryRequest, object);
            }));
  }

  @Override
  protected Inventory mapUpdate(Inventory oldData, Inventory newData) {
    newData.setId(oldData.getId());
    newData.setCreatedBy(oldData.getCreatedBy());
    newData.setCreatedAt(oldData.getCreatedAt());
    return newData;
  }

//  @Override
//  public Uni<PageWrapper<Inventory>> findAll(Integer page, Integer size) {
//    return super.findAll(page, size)
//        .onItem().produceUni(inventories -> {
//          if (!inventories.getContent().isEmpty()) {
//            List<String> cpIds = inventories.getContent().stream()
//                .map(Inventory::getCancellationPolicyId).collect(
//                    Collectors.toList());
//            return cancellationPolicyService.findAllByIdsAndDeleted(cpIds, false)
//                .onItem().apply(cps -> {
//                  Map<String, CancellationPolicy> cpMap = new HashMap<>();
//                  cps.forEach(cp -> cpMap.put(cp.getId().toString(), cp));
//                  inventories.getContent().forEach(
//                      inv -> inv.setCancellationPolicy(cpMap.get(inv.getCancellationPolicyId())));
//                  return inventories;
//                });
//          }
//
//          return Uni.createFrom().item(inventories);
//        });
//  }

//  @Override
//  public Uni<Inventory> findById(String id) {
//    return super.findById(id)
//        .onItem()
//        .produceUni(inv -> cancellationPolicyService.findById(inv.getCancellationPolicyId())
//            .onItem().apply(cp -> {
//              inv.setCancellationPolicy(cp);
//
//              return inv;
//            }));
//  }

  @Override
  public Uni<List<Inventory>> updateAllotment(MandatoryRequest mandatoryRequest,
      List<AllotmentDto> allotmentDtos) {
    return Uni.createFrom().deferred(() -> {
      List<String> inventoryIds = allotmentDtos.stream().map(BaseDto::getId)
          .collect(Collectors.toList());

      return repository().findAllByIdsAndDeleted(inventoryIds, false)
          .onItem().ifNull().failWith(new BusinessLogicException(ResponseCode.DATA_NOT_EXIST,
              Collections.singletonList(ResponseCode.DATA_NOT_EXIST.getMessage())))
          .onItem().apply(inventories -> {
            Map<String, Inventory> map = new HashMap<>();
            inventories.forEach(inv -> map.put(inv.getId().toString(), inv));
            return map;
          })
          .onItem().produceUni(inventories -> {
            if (allotmentDtos.size() != inventories.size()) {
              return Uni.createFrom()
                  .failure(new BusinessLogicException(ResponseCode.DATA_NOT_EXIST,
                      Collections.singletonList(ResponseCode.DATA_NOT_EXIST.getMessage())));
            }

            List<Inventory> inventorySave = addOrDeductInventory(allotmentDtos, inventories);

            return this.upsert(mandatoryRequest, inventorySave, Action.UPDATE).onItem()
                .apply(t -> inventorySave)
                .onItem().produceUni(this::mapCancellationPolicies);
          });
    }).onFailure().invoke(t -> log.error("Error update allotment request = {}", allotmentDtos, t));
  }

  @Override
  public Uni<PageWrapper<Inventory>> findAllByCustomFields(MandatoryRequest mandatoryRequest,
      SearchDto searchDto) {
    List<Tuple3<String, MongoComparator, Object>> fields = new ArrayList<>();
    if (!StringUtil.isNullOrEmpty(searchDto.getDeparture())) {
      fields.add(Tuple3.of("departureLocation", MongoComparator.EQUAL, searchDto.getDeparture()));
    }
    if (!StringUtil.isNullOrEmpty(searchDto.getArrival())) {
      fields.add(Tuple3.of("arrivalLocation", MongoComparator.EQUAL, searchDto.getArrival()));
    }
    if (!StringUtil.isNullOrEmpty(searchDto.getDate())) {
      fields.add(Tuple3.of("date", MongoComparator.EQUAL, Instant.parse(searchDto.getDate())));
    }
    if (searchDto.getPassenger() > 0) {
      fields.add(
          Tuple3.of("allotment", MongoComparator.GREATER_THAN_EQUAL, searchDto.getPassenger()));
    }

    return Uni.createFrom().deferred(() -> {
      ReactivePanacheQuery<Inventory> q = repository().findByCustomFields(
          Page.of(searchDto.getPage(), searchDto.getSize()),
          fields);
      return Uni.combine().all().unis(q.count(), q.list(), q.pageCount())
          .asTuple().onItem().apply(tuple -> PageWrapper.<Inventory>builder()
              .pageInfo(PageInfo.builder()
                  .dataCount(tuple.getItem2().size()).pageIndex(searchDto.getPage())
                  .pageCount(tuple.getItem3())
                  .totalData(tuple.getItem1())
                  .build())
              .content(tuple.getItem2())
              .build());
    });
  }

  private Uni<List<Inventory>> mapCancellationPolicies(List<Inventory> invs) {
    return Uni.createFrom()
        .deferred(() -> cancellationPolicyService.findAllByIdsAndDeleted(invs.stream().map(
            Inventory::getCancellationPolicyId).collect(Collectors.toList()), false)
            .onItem().apply(cps -> {
              Map<String, CancellationPolicy> cpMap = new HashMap<>();
              cps.forEach(cp -> cpMap.put(cp.getId().toString(), cp));

              invs.forEach(i -> i.setCancellationPolicy(cpMap.get(i.getCancellationPolicyId())));

              return invs;
            }));
  }

  private List<Inventory> addOrDeductInventory(List<AllotmentDto> allotmentDtos,
      Map<String, Inventory> inventories) {
    List<Inventory> inventorySave = new ArrayList<>();
    allotmentDtos.forEach(dto -> {
      Inventory inventory = inventories.get(dto.getId());
      switch (dto.getType()) {
        case ADD:
          inventory.setAllotment(inventory.getAllotment() + dto.getAllotment());
          inventorySave.add(inventory);
          break;
        case DEDUCT:
          if (inventory.getAllotment() - dto.getAllotment() >= 0) {
            inventory.setAllotment(inventory.getAllotment() - dto.getAllotment());
            inventorySave.add(inventory);
          } else {
            throw new BusinessLogicException("NOT_ENOUGH_ALLOTMENT",
                "Remaining allotment after deducted is under 0", Collections.singletonList(
                "Remaining allotment for inventory id " + dto.getId() + " is under 0"));
          }
          break;
        default:
          break;
      }
    });

    return inventorySave;
  }
}

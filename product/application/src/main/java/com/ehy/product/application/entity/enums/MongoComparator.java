package com.ehy.product.application.entity.enums;

import lombok.Getter;

@Getter
public enum MongoComparator {
    EQUAL("="),
    LESS_THAN("<"),
    LESS_THAN_EQUAL("<="),
    GREATER_THAN(">"),
    GREATER_THAN_EQUAL(">=");

    private String label;
    MongoComparator(String label) {
        this.label = label;
    }
}

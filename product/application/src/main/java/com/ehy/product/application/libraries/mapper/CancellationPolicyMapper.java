package com.ehy.product.application.libraries.mapper;

import com.ehy.product.application.entity.constant.BaseMongoFields;
import com.ehy.product.application.entity.domain.CancellationPolicy;
import com.ehy.product.dto.CancellationPolicyDto;
import java.util.List;
import org.mapstruct.IterableMapping;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;
import org.mapstruct.Named;

@Mapper(componentModel = "cdi")
public interface CancellationPolicyMapper extends BaseMapper {

  @Mapping(target = "id", source = "cancellationPolicyDto.id", qualifiedByName = "stringToObjectId")
  CancellationPolicy toDomain(CancellationPolicyDto cancellationPolicyDto);

  @Named("toDto")
  @Mapping(target = "id", source = "cancellationPolicy.id", qualifiedByName = "objectIdToString")
  CancellationPolicyDto toDto(CancellationPolicy cancellationPolicy);

  @IterableMapping(qualifiedByName = "toDto")
  List<CancellationPolicyDto> toDtoList(List<CancellationPolicy> cancellationPolicies);

  @Mappings({
      @Mapping(target = "id", source = "original.id"),
      @Mapping(target = "refundable", source = "newData.refundable"),
      @Mapping(target = "percentage", source = "newData.percentage"),
      @Mapping(target = "daysBefore", source = "newData.daysBefore"),
      @Mapping(target = BaseMongoFields.CREATED_AT, source = "original.createdAt"),
      @Mapping(target = BaseMongoFields.CREATED_BY, source = "original.createdBy"),
      @Mapping(target = BaseMongoFields.UPDATED_AT, source = "original.updatedAt"),
      @Mapping(target = BaseMongoFields.UPDATED_BY, source = "original.updatedBy"),
      @Mapping(target = BaseMongoFields.DELETED, source = "original.deleted")
  })
  CancellationPolicy from(CancellationPolicy original, CancellationPolicy newData);
}

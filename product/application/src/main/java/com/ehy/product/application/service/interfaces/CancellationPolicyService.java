package com.ehy.product.application.service.interfaces;

import com.ehy.product.application.entity.domain.CancellationPolicy;

public interface CancellationPolicyService extends BaseResourceService<CancellationPolicy> {

}

package com.ehy.product.application.dao.impl;

import com.ehy.product.application.dao.interfaces.SystemParameterRepository;
import com.ehy.product.application.entity.domain.SystemParameter;
import javax.enterprise.context.ApplicationScoped;

@ApplicationScoped
public class SystemParameterRepositoryImpl extends BaseRepositoryImpl<SystemParameter> implements
    SystemParameterRepository {

}

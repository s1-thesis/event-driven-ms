package com.ehy.product.application.dao.impl;

import com.ehy.common.model.MandatoryRequest;
import com.ehy.product.application.dao.interfaces.BaseRepository;
import com.ehy.product.application.entity.constant.BaseMongoFields;
import com.ehy.product.application.entity.domain.BaseMongo;
import com.ehy.product.application.entity.enums.Action;
import com.ehy.product.application.entity.enums.MongoComparator;
import com.ehy.product.application.libraries.helper.CommonHelper;
import io.quarkus.mongodb.panache.reactive.ReactivePanacheQuery;
import io.quarkus.panache.common.Page;
import io.quarkus.panache.common.Sort;
import io.smallrye.mutiny.Multi;
import io.smallrye.mutiny.Uni;
import io.smallrye.mutiny.tuples.Tuple2;
import io.smallrye.mutiny.tuples.Tuple3;
import org.bson.types.ObjectId;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class BaseRepositoryImpl<T extends BaseMongo> implements BaseRepository<T> {

  @Override
  public Multi<T> findAllByDeleted(boolean deleted) {
    return this.find(BaseMongoFields.DELETED + " = ?1", deleted, Sort.ascending(BaseMongoFields.ID))
        .stream();
  }

  @Override
  public ReactivePanacheQuery<T> findAllByDeleted(boolean deleted, Page page) {
    return this.find(BaseMongoFields.DELETED + " = ?1", deleted, Sort.ascending(BaseMongoFields.ID))
        .page(page);
  }

  @Override
  public Uni<T> findByIdAndDeleted(String id, boolean deleted) {
    return this.find(BaseMongoFields.ID + " = ?1 and " + BaseMongoFields.DELETED + " = ?2",
        new ObjectId(id), deleted).firstResult();
  }

  @Override
  public Uni<Long> countByDeleted(boolean deleted) {
    return this.count(BaseMongoFields.DELETED + " = ?1", deleted);
  }

  @Override
  public Uni<Void> upsert(MandatoryRequest mandatoryRequest, T t, Action action) {
    return this.persistOrUpdate(CommonHelper.setBaseMongoFields(mandatoryRequest, t, action));
  }

  @Override
  public Uni<Void> upsert(MandatoryRequest mandatoryRequest, List<T> t, Action action) {
    List<T> list = t.stream()
        .map(item -> CommonHelper.setBaseMongoFields(mandatoryRequest, item, action)).collect(
            Collectors.toList());
    return this.persistOrUpdate(list);
  }

  /**
   * BARBAR
   */
  @Override
  public Uni<List<T>> findAllByIdsAndDeleted(List<String> ids, boolean deleted) {
    StringBuilder idQuery = new StringBuilder();

    for (int i = 0; i < ids.size(); i++) {
      idQuery.append("ObjectId(\"").append(ids.get(i)).append("\")");
      if (i != ids.size() - 1) {
        idQuery.append(",");
      }
    }

    return this
        .list("{" + BaseMongoFields.ID + ": {$in: [ " + idQuery + "]}, " + BaseMongoFields.DELETED
                + ": ?1" + "}",
            deleted);
  }

  @Override
  public ReactivePanacheQuery<T> findByCustomFields(Page page, List<Tuple3<String, MongoComparator, Object>> fields) {
    Tuple2<StringBuilder, Map<String, Object>> q = buildQuery(fields);
    if (fields.size() > 0) return this.find(q.getItem1().toString(), q.getItem2()).page(page);
    else return findAllByDeleted(false, page);
  }

  public Tuple2<StringBuilder, Map<String, Object>> buildQuery(List<Tuple3<String, MongoComparator, Object>> fields) {
    StringBuilder sb = new StringBuilder();
    Map<String, Object> mapper = new HashMap<>();
    for (int i = 0; i < fields.size(); i++) {
      String fieldName = fields.get(i).getItem1();
      mapper.put(fieldName, fields.get(i).getItem3());
      sb.append(fieldName).append(String.format(" %s :%s", fields.get(i).getItem2().getLabel(), fieldName));
      if (i != fields.size() - 1) {
        sb.append(" and ");
      }
    }

    return Tuple2.of(sb, mapper);
  }
}

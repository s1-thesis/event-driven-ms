package com.ehy.product.application.dao.impl;

import com.ehy.product.application.dao.interfaces.CancellationPolicyRepository;
import com.ehy.product.application.entity.domain.CancellationPolicy;
import javax.enterprise.context.ApplicationScoped;

@ApplicationScoped
public class CancellationPolicyRepositoryImpl extends
    BaseRepositoryImpl<CancellationPolicy> implements CancellationPolicyRepository {

}

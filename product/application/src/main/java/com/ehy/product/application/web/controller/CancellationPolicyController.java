package com.ehy.product.application.web.controller;

import com.ehy.common.model.BaseResponse;
import com.ehy.common.model.MandatoryRequest;
import com.ehy.common.model.PageWrapper;
import com.ehy.product.application.entity.constant.ApiPath;
import com.ehy.product.application.libraries.mapper.CancellationPolicyMapper;
import com.ehy.product.application.service.interfaces.CancellationPolicyService;
import com.ehy.product.dto.CancellationPolicyDto;
import io.smallrye.mutiny.Uni;
import java.util.List;
import javax.inject.Inject;
import javax.validation.Valid;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.ws.rs.BeanParam;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import org.eclipse.microprofile.openapi.annotations.parameters.RequestBody;
import org.eclipse.microprofile.openapi.annotations.tags.Tag;
import org.eclipse.microprofile.openapi.annotations.tags.Tags;
import org.jboss.resteasy.annotations.jaxrs.PathParam;
import org.jboss.resteasy.annotations.jaxrs.QueryParam;

@Path(ApiPath.BASE_PATH_CANCELLATION_POLICIES)
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
@Tags(value = @Tag(name = "Cancellation Policy", description = "CRUD for cancellation policy"))
public class CancellationPolicyController extends BaseResource {
  
  @Inject
  CancellationPolicyService cancellationPolicyService;

  @Inject
  CancellationPolicyMapper mapper;

  @GET
  public Uni<BaseResponse<PageWrapper<CancellationPolicyDto>>> findAll(
      @BeanParam @Valid MandatoryRequest mandatoryRequest,
      @Valid @DefaultValue("0") @Min(0) @NotNull @QueryParam Integer page,
      @Valid @DefaultValue("10") @Min(0) @Max(100) @NotNull @QueryParam Integer size) {
    return buildResponse(cancellationPolicyService.findAll(page, size)
        .onItem().apply(cp -> {
          List<CancellationPolicyDto> systemParameterDtoList = mapper.toDtoList(cp.getContent());
          return PageWrapper.<CancellationPolicyDto>builder().pageInfo(cp.getPageInfo())
              .content(systemParameterDtoList).build();
        }));
  }

  @GET
  @Path(ApiPath.APPEND_ID)
  public Uni<BaseResponse<CancellationPolicyDto>> findOne(
      @BeanParam @Valid MandatoryRequest mandatoryRequest,
      @PathParam String id) {
    return buildResponse(
        cancellationPolicyService.findById(id).onItem().apply(item -> mapper.toDto(item)));
  }

  @POST
  public Uni<BaseResponse<Boolean>> create(
      @BeanParam @Valid MandatoryRequest mandatoryRequest,
      @RequestBody @Valid CancellationPolicyDto cancellationPolicy) {
    return buildResponse(
        cancellationPolicyService.create(mandatoryRequest, mapper.toDomain(cancellationPolicy)));
  }

  @PUT
  @Path(ApiPath.APPEND_ID)
  public Uni<BaseResponse<Boolean>> update(
      @BeanParam @Valid MandatoryRequest mandatoryRequest,
      @PathParam String id,
      @RequestBody @Valid CancellationPolicyDto cancellationPolicy) {
    cancellationPolicy.setId(id);
    return buildResponse(
        cancellationPolicyService.update(mandatoryRequest, mapper.toDomain(cancellationPolicy)));
  }

  @DELETE
  @Path(ApiPath.APPEND_ID)
  public Uni<BaseResponse<Boolean>> delete(
      @BeanParam @Valid MandatoryRequest mandatoryRequest,
      @PathParam String id) {
    return buildResponse(cancellationPolicyService.delete(mandatoryRequest, id));
  }
}

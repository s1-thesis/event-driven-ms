package com.ehy.product.application.entity.constant;

public class ApiPath {

  /**
   * Rename this with service name
   */
  public static final String BASE_PATH = "/product-service";

  public static final String APPEND_CANCELLATION_POLICIES = "/cancellation-policies";
  public static final String APPEND_INVENTORIES = "/inventories";
  public static final String APPEND_SYSTEM_PARAMETERS = "/system-parameters";
  public static final String APPEND_ID = "/{id}";
  public static final String APPEND_ALLOTMENT = "/allotment";

  /**
   * Base path
   */
  public static final String BASE_PATH_SYSTEM_PARAMETERS = BASE_PATH + APPEND_SYSTEM_PARAMETERS;
  public static final String BASE_PATH_CANCELLATION_POLICIES = BASE_PATH + APPEND_CANCELLATION_POLICIES;
  public static final String BASE_PATH_INVENTORIES = BASE_PATH + APPEND_INVENTORIES;
}

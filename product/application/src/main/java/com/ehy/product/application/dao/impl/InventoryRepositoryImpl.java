package com.ehy.product.application.dao.impl;

import com.ehy.product.application.dao.interfaces.InventoryRepository;
import com.ehy.product.application.entity.domain.Inventory;
import javax.enterprise.context.ApplicationScoped;

@ApplicationScoped
public class InventoryRepositoryImpl extends BaseRepositoryImpl<Inventory> implements
    InventoryRepository {

}

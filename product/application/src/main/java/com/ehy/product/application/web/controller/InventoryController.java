package com.ehy.product.application.web.controller;

import com.ehy.common.model.BaseResponse;
import com.ehy.common.model.MandatoryRequest;
import com.ehy.common.model.PageWrapper;
import com.ehy.product.application.entity.constant.ApiPath;
import com.ehy.product.application.libraries.mapper.InventoryMapper;
import com.ehy.product.application.service.interfaces.CancellationPolicyService;
import com.ehy.product.application.service.interfaces.InventoryService;
import com.ehy.product.dto.AllotmentDto;
import com.ehy.product.dto.InventoryDto;
import com.ehy.product.dto.SearchDto;
import io.smallrye.mutiny.Uni;
import java.util.List;
import javax.inject.Inject;
import javax.validation.Valid;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.ws.rs.BeanParam;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import org.eclipse.microprofile.openapi.annotations.parameters.RequestBody;
import org.eclipse.microprofile.openapi.annotations.tags.Tag;
import org.eclipse.microprofile.openapi.annotations.tags.Tags;
import org.jboss.resteasy.annotations.jaxrs.PathParam;
import org.jboss.resteasy.annotations.jaxrs.QueryParam;

@Path(ApiPath.BASE_PATH_INVENTORIES)
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
@Tags(value = @Tag(name = "Inventory", description = "CRUD for inventory"))
public class InventoryController extends BaseResource {

  @Inject
  InventoryService inventoryService;

  @Inject
  CancellationPolicyService cancellationPolicyService;

  @Inject
  InventoryMapper mapper;

  @GET
  public Uni<BaseResponse<PageWrapper<InventoryDto>>> findAll(
          @BeanParam @Valid MandatoryRequest mandatoryRequest,
          @Valid @DefaultValue("0") @Min(0) @NotNull @QueryParam Integer page,
          @Valid @DefaultValue("10") @Min(0) @Max(100) @NotNull @QueryParam Integer size,
          @Valid @QueryParam String departure,
          @Valid @QueryParam String arrival,
          @Valid @QueryParam String date,
          @Valid @DefaultValue("0") @QueryParam Integer passenger) {

    return buildResponse(inventoryService.findAllByCustomFields(mandatoryRequest, SearchDto.builder()
            .page(page)
            .size(size)
            .departure(departure)
            .arrival(arrival)
            .date(date)
            .passenger(passenger)
            .build())
        .onItem().apply(inventory -> {
          List<InventoryDto> systemParameterDtoList = mapper.toDtoList(inventory.getContent());
          return PageWrapper.<InventoryDto>builder().pageInfo(inventory.getPageInfo())
              .content(systemParameterDtoList).build();
        }));
  }

  @GET
  @Path(ApiPath.APPEND_ID)
  public Uni<BaseResponse<InventoryDto>> findOne(
      @BeanParam @Valid MandatoryRequest mandatoryRequest,
      @PathParam String id) {
    return buildResponse(
        inventoryService.findById(id).onItem().apply(item -> mapper.toDto(item)));
  }

  @POST
  public Uni<BaseResponse<Boolean>> create(
      @BeanParam @Valid MandatoryRequest mandatoryRequest,
      @RequestBody @Valid InventoryDto inventoryDto) {
    return buildResponse(cancellationPolicyService.findById(inventoryDto.getCancellationPolicyId())
        .onItem().produceUni(
            cp -> inventoryService.create(mandatoryRequest, mapper.toDomain(inventoryDto))));
  }

  @PUT
  @Path(ApiPath.APPEND_ID)
  public Uni<BaseResponse<Boolean>> update(
      @BeanParam @Valid MandatoryRequest mandatoryRequest,
      @PathParam String id,
      @RequestBody @Valid InventoryDto inventoryDto) {
    inventoryDto.setId(id);
    return buildResponse(cancellationPolicyService.findById(inventoryDto.getCancellationPolicyId())
        .onItem().produceUni(cp ->
            inventoryService.update(mandatoryRequest, mapper.toDomain(inventoryDto))));
  }

  @DELETE
  @Path(ApiPath.APPEND_ID)
  public Uni<BaseResponse<Boolean>> delete(
      @BeanParam @Valid MandatoryRequest mandatoryRequest,
      @PathParam String id) {
    return buildResponse(inventoryService.delete(mandatoryRequest, id));
  }

  @POST
  @Path(ApiPath.APPEND_ALLOTMENT)
  public Uni<BaseResponse<List<InventoryDto>>> updateAllotment(
      @BeanParam @Valid MandatoryRequest mandatoryRequest,
      @RequestBody @Valid List<AllotmentDto> allotmentDto) {
    return buildResponse(inventoryService.updateAllotment(mandatoryRequest, allotmentDto).onItem()
            .apply(inventories -> mapper.toDtoList(inventories)));
  }
}

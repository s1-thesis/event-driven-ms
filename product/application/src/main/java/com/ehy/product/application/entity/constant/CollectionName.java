package com.ehy.product.application.entity.constant;

public class CollectionName {

  public static final String SYSTEM_PARAMETER = "system_parameter";
  public static final String CANCELLATION_POLICY = "cancellation_policy";
  public static final String INVENTORY = "inventory";
}

package com.ehy.product.application.entity.enums;

public enum Action {
  CREATE, UPDATE, DELETE
}

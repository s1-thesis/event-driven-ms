package com.ehy.product.application.service.interfaces;

import com.ehy.common.model.MandatoryRequest;
import com.ehy.common.model.PageWrapper;
import com.ehy.product.application.entity.domain.Inventory;
import com.ehy.product.dto.AllotmentDto;
import com.ehy.product.dto.SearchDto;
import io.smallrye.mutiny.Uni;
import java.util.List;

public interface InventoryService extends BaseResourceService<Inventory> {

  Uni<List<Inventory>> updateAllotment(MandatoryRequest mandatoryRequest, List<AllotmentDto> allotmentDtos);

  Uni<PageWrapper<Inventory>> findAllByCustomFields(MandatoryRequest mandatoryRequest, SearchDto searchDto);
}

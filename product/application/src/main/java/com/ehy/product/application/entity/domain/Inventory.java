package com.ehy.product.application.entity.domain;

import com.ehy.product.application.entity.constant.CollectionName;
import io.quarkus.mongodb.panache.MongoEntity;
import java.time.Instant;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;
import org.bson.types.ObjectId;

@Data
@NoArgsConstructor
@EqualsAndHashCode(callSuper = true)
@ToString(callSuper = true)
@MongoEntity(collection = CollectionName.INVENTORY)
public class Inventory extends BaseMongo {

  private String airlineCode;
  private Instant date;
  private String departureTime;
  private String arrivalTime;
  private String departureLocation;
  private String arrivalLocation;
  private Integer allotment;
  private Pricing pricing;
  private String cancellationPolicyId;
  private CancellationPolicy cancellationPolicy;

  @Builder
  public Inventory(ObjectId id, String createdBy, String updatedBy, Instant createdAt,
      Instant updatedAt, boolean deleted, String airlineCode, Instant date,
      String departureTime, String arrivalTime, String departureLocation,
      String arrivalLocation, Integer allotment,
      Pricing pricing, String cancellationPolicyId,
      CancellationPolicy cancellationPolicy) {
    super(id, createdBy, updatedBy, createdAt, updatedAt, deleted);
    this.airlineCode = airlineCode;
    this.date = date;
    this.departureTime = departureTime;
    this.arrivalTime = arrivalTime;
    this.departureLocation = departureLocation;
    this.arrivalLocation = arrivalLocation;
    this.allotment = allotment;
    this.pricing = pricing;
    this.cancellationPolicyId = cancellationPolicyId;
    this.cancellationPolicy = cancellationPolicy;
  }
}

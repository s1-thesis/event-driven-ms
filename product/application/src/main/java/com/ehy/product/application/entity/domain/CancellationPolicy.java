package com.ehy.product.application.entity.domain;

import com.ehy.product.application.entity.constant.CollectionName;
import io.quarkus.mongodb.panache.MongoEntity;
import java.time.Instant;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;
import org.bson.types.ObjectId;

@Data
@NoArgsConstructor
@EqualsAndHashCode(callSuper = true)
@ToString(callSuper = true)
@MongoEntity(collection = CollectionName.CANCELLATION_POLICY)
public class CancellationPolicy extends BaseMongo {

  private boolean refundable;
  private Double percentage;
  private Integer daysBefore;

  @Builder
  public CancellationPolicy(ObjectId id, String createdBy, String updatedBy,
      Instant createdAt, Instant updatedAt, boolean deleted, boolean refundable,
      Double percentage, Integer daysBefore) {
    super(id, createdBy, updatedBy, createdAt, updatedAt, deleted);
    this.refundable = refundable;
    this.percentage = percentage;
    this.daysBefore = daysBefore;
  }
}

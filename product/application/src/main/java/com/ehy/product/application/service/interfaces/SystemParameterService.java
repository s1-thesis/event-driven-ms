package com.ehy.product.application.service.interfaces;

import com.ehy.product.application.entity.domain.SystemParameter;

public interface SystemParameterService extends BaseResourceService<SystemParameter> {

}

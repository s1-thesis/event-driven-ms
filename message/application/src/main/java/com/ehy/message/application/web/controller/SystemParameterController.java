package com.ehy.message.application.web.controller;

import com.ehy.common.model.BaseResponse;
import com.ehy.common.model.MandatoryRequest;
import com.ehy.common.model.PageWrapper;
import com.ehy.message.application.entity.constant.ApiPath;
import com.ehy.message.application.libraries.mapper.SystemParameterMapper;
import com.ehy.message.application.service.interfaces.SystemParameterService;
import com.ehy.message.dto.SystemParameterDto;
import io.smallrye.mutiny.Uni;
import java.util.List;
import javax.inject.Inject;
import javax.validation.Valid;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.ws.rs.BeanParam;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import org.eclipse.microprofile.openapi.annotations.parameters.RequestBody;
import org.eclipse.microprofile.openapi.annotations.tags.Tag;
import org.eclipse.microprofile.openapi.annotations.tags.Tags;
import org.jboss.resteasy.annotations.jaxrs.PathParam;
import org.jboss.resteasy.annotations.jaxrs.QueryParam;

@Path(ApiPath.BASE_PATH_SYSTEM_PARAMETERS)
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
@Tags(value = @Tag(name = "System Parameter", description = "CRUD for system parameter"))
public class SystemParameterController extends BaseResource {

  @Inject
  SystemParameterService systemParameterService;
  @Inject
  SystemParameterMapper mapper;

  @GET
  public Uni<BaseResponse<PageWrapper<SystemParameterDto>>> findAll(
      @BeanParam @Valid MandatoryRequest mandatoryRequest,
      @Valid @DefaultValue("0") @Min(0) @NotNull @QueryParam Integer page,
      @Valid @DefaultValue("10") @Min(0) @Max(100) @NotNull @QueryParam Integer size) {
    return buildResponse(systemParameterService.findAll(page, size)
        .onItem().apply(sysPar -> {
          List<SystemParameterDto> systemParameterDtoList = mapper.toDtoList(sysPar.getContent());
          return PageWrapper.<SystemParameterDto>builder().pageInfo(sysPar.getPageInfo())
              .content(systemParameterDtoList).build();
        }));
  }

  @GET
  @Path(ApiPath.APPEND_ID)
  public Uni<BaseResponse<SystemParameterDto>> findOne(
      @BeanParam @Valid MandatoryRequest mandatoryRequest,
      @PathParam String id) {
    return buildResponse(
        systemParameterService.findById(id).onItem().apply(item -> mapper.toDto(item)));
  }

  @POST
  public Uni<BaseResponse<Boolean>> create(
      @BeanParam @Valid MandatoryRequest mandatoryRequest,
      @RequestBody @Valid SystemParameterDto systemParameter) {
    return buildResponse(
        systemParameterService.create(mandatoryRequest, mapper.toDomain(systemParameter)));
  }

  @PUT
  @Path(ApiPath.APPEND_ID)
  public Uni<BaseResponse<Boolean>> update(
      @BeanParam @Valid MandatoryRequest mandatoryRequest,
      @RequestBody @Valid SystemParameterDto systemParameter,
      @PathParam String id) {
    systemParameter.setId(id);
    return buildResponse(
        systemParameterService.update(mandatoryRequest, mapper.toDomain(systemParameter)));
  }

  @DELETE
  @Path(ApiPath.APPEND_ID)
  public Uni<BaseResponse<Boolean>> delete(
      @BeanParam @Valid MandatoryRequest mandatoryRequest,
      @PathParam String id) {
    return buildResponse(systemParameterService.delete(mandatoryRequest, id));
  }
}

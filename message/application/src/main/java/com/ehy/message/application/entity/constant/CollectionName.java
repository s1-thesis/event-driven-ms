package com.ehy.message.application.entity.constant;

public class CollectionName {

  public static final String SYSTEM_PARAMETER = "system_parameter";
  public static final String EMAIL_LOG = "email_log";
}

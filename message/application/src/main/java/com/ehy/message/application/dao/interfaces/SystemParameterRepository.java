package com.ehy.message.application.dao.interfaces;

import com.ehy.message.application.entity.domain.SystemParameter;

public interface SystemParameterRepository extends BaseRepository<SystemParameter> {

}

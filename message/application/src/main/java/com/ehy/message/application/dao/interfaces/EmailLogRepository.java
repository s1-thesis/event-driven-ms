package com.ehy.message.application.dao.interfaces;

import com.ehy.message.application.entity.domain.EmailLog;

public interface EmailLogRepository extends BaseRepository<EmailLog> {

}

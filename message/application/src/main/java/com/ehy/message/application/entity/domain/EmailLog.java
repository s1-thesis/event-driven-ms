package com.ehy.message.application.entity.domain;

import com.ehy.message.application.entity.constant.CollectionName;
import com.ehy.message.application.entity.rest.mailgun.EmailResponse;
import com.ehy.message.dto.EmailDto;
import io.quarkus.mongodb.panache.MongoEntity;
import java.time.Instant;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;
import org.bson.types.ObjectId;

@Data
@NoArgsConstructor
@EqualsAndHashCode(callSuper = true)
@ToString(callSuper = true)
@MongoEntity(collection = CollectionName.EMAIL_LOG)
public class EmailLog extends BaseMongo {

  private String user;
  private EmailDto request;
  private EmailResponse response;

  @Builder
  public EmailLog(ObjectId id, String createdBy, String updatedBy,
      Instant createdAt, Instant updatedAt, boolean deleted,
      String user, EmailDto request, EmailResponse response) {
    super(id, createdBy, updatedBy, createdAt, updatedAt, deleted);
    this.user = user;
    this.request = request;
    this.response = response;
  }
}

package com.ehy.message.application.service.impl;

import com.ehy.message.application.dao.interfaces.BaseRepository;
import com.ehy.message.application.dao.interfaces.SystemParameterRepository;
import com.ehy.message.application.entity.domain.SystemParameter;
import com.ehy.message.application.service.interfaces.SystemParameterService;
import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import lombok.extern.slf4j.Slf4j;

@ApplicationScoped
@Slf4j
public class SystemParameterServiceImpl extends BaseResourceServiceImpl<SystemParameter> implements SystemParameterService {

  @Inject
  SystemParameterRepository systemParameterRepository;

  @Override
  protected BaseRepository<SystemParameter> repository() {
    return systemParameterRepository;
  }

  @Override
  protected SystemParameter mapUpdate(SystemParameter oldData, SystemParameter newData) {
    oldData.setVariable(newData.getVariable());
    oldData.setValue(newData.getValue());
    oldData.setDescription(newData.getDescription());
    return oldData;
  }
}

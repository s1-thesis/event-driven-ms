package com.ehy.message.application.rest;

import com.ehy.common.configuration.RestLoggingInterceptor;
import com.ehy.message.application.entity.constant.MailGunApiPath;
import com.ehy.message.application.entity.rest.mailgun.EmailResponse;
import io.smallrye.mutiny.Uni;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import org.eclipse.microprofile.rest.client.annotation.RegisterProvider;
import org.eclipse.microprofile.rest.client.inject.RegisterRestClient;
import org.jboss.resteasy.annotations.jaxrs.QueryParam;

@Path(MailGunApiPath.V3)
@Produces(MediaType.APPLICATION_JSON)
@RegisterProvider(RestLoggingInterceptor.class)
@RegisterRestClient
public interface MailGunService {

  @POST
  @Path(MailGunApiPath.SEND_EMAIL)
  Uni<EmailResponse> sendEmail(@HeaderParam("Authorization") String auth, @QueryParam("from") String from,
      @QueryParam("to") String to, @QueryParam("subject") String subject,
      @QueryParam("text") String text);
}

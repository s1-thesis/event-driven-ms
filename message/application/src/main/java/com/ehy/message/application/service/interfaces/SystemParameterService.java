package com.ehy.message.application.service.interfaces;

import com.ehy.message.application.entity.domain.SystemParameter;

public interface SystemParameterService extends BaseResourceService<SystemParameter> {

}

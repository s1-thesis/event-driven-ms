package com.ehy.message.application.listener.kafka;

import com.ehy.common.model.MandatoryRequest;
import com.ehy.message.application.service.interfaces.EmailService;
import com.ehy.message.dto.EmailDto;
import java.util.concurrent.CompletionStage;
import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import lombok.extern.slf4j.Slf4j;
import org.eclipse.microprofile.reactive.messaging.Incoming;
import org.eclipse.microprofile.reactive.messaging.Message;

@ApplicationScoped
@Slf4j
public class MessageListener {

  @Inject
  EmailService emailService;

  @Incoming("email")
  public CompletionStage<Void> emailListener(EmailDto emailDto) {
    log.info("[KAFKA] Send email request = {}", emailDto);
    return emailService
        .sendEmail(MandatoryRequest.builder().username(emailDto.getUsername()).build(),
            emailDto).subscribeAsCompletionStage()
        .thenAccept(t -> log.info("Successfully processing email request = {}", emailDto));
  }
}

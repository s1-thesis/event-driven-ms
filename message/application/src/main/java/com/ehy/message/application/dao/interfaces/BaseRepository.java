package com.ehy.message.application.dao.interfaces;

import com.ehy.common.model.MandatoryRequest;
import com.ehy.message.application.entity.enums.Action;
import io.quarkus.mongodb.panache.reactive.ReactivePanacheMongoRepository;
import io.quarkus.mongodb.panache.reactive.ReactivePanacheQuery;
import io.quarkus.panache.common.Page;
import io.smallrye.mutiny.Multi;
import io.smallrye.mutiny.Uni;

public interface BaseRepository<T> extends ReactivePanacheMongoRepository<T> {

  Multi<T> findAllByDeleted(boolean deleted);

  ReactivePanacheQuery<T> findAllByDeleted(boolean deleted, Page page);

  Uni<T> findByIdAndDeleted(String id, boolean deleted);

  Uni<Long> countByDeleted(boolean deleted);

  Uni<Void> upsert(MandatoryRequest mandatoryRequest, T t, Action action);
}

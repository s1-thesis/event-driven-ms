package com.ehy.message.application.entity.enums;

public enum Action {
  CREATE, UPDATE, DELETE
}

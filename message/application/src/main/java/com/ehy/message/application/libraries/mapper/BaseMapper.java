package com.ehy.message.application.libraries.mapper;

import io.netty.util.internal.StringUtil;
import org.bson.types.ObjectId;
import org.mapstruct.Named;

public interface BaseMapper {

  @Named("objectIdToString")
  default String map(ObjectId value) {
    return value.toString();
  }

  @Named("stringToObjectId")
  default ObjectId map(String value) {
    if (StringUtil.isNullOrEmpty(value)) {
      return null;
    }
    return new ObjectId(value);
  }
}

package com.ehy.message.application.service.impl;

import com.ehy.common.helper.CommonHelper;
import com.ehy.common.model.MandatoryRequest;
import com.ehy.message.application.dao.interfaces.BaseRepository;
import com.ehy.message.application.dao.interfaces.EmailLogRepository;
import com.ehy.message.application.entity.domain.EmailLog;
import com.ehy.message.application.entity.rest.mailgun.EmailResponse;
import com.ehy.message.application.rest.MailGunService;
import com.ehy.message.application.service.interfaces.EmailService;
import com.ehy.message.dto.EmailDto;
import io.smallrye.mutiny.Uni;
import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import lombok.extern.slf4j.Slf4j;
import org.eclipse.microprofile.config.inject.ConfigProperty;
import org.eclipse.microprofile.rest.client.inject.RestClient;

@ApplicationScoped
@Slf4j
public class EmailServiceImpl extends BaseResourceServiceImpl<EmailLog> implements EmailService {

  @Inject
  @RestClient
  MailGunService mailGunService;

  @ConfigProperty(name = "mailgun.api.key")
  String apiKey;

  @Inject
  EmailLogRepository emailLogRepository;

  @Override
  protected BaseRepository<EmailLog> repository() {
    return emailLogRepository;
  }

  @Override
  protected EmailLog mapUpdate(EmailLog oldData, EmailLog newData) {
    return null;
  }

  @Override
  public Uni<String> sendEmail(MandatoryRequest mandatoryRequest, EmailDto request) {
    return Uni.createFrom().deferred(() -> mailGunService
        .sendEmail("Basic " + CommonHelper.buildBasicAuthHeader("api", apiKey), request.getFrom(),
            request.getTo(), request.getSubject(), request.getText())
//      return emailTest()
        .onFailure().invoke(t -> log
        .error("Error sending message, from = {}, to = {}, subject = {}, text = {}",
            request.getFrom(),
            request.getTo(), request.getSubject(), request.getText(), t))
        .onItem().produceUni(response -> create(mandatoryRequest,
        EmailLog.builder().user(mandatoryRequest.getUsername())
            .request(request).response(response).build()).map(t -> response.getMessage())));
  }

  private Uni<EmailResponse> emailTest() {
    return Uni.createFrom().item(EmailResponse.builder().message("hehe").build());
  }
}

package com.ehy.message.application.entity.constant;

public class MailGunApiPath {

  public static final String V3 = "/v3";
  public static final String DOMAIN_URL = "/sandbox7a77fa03559542a89a9b09cf5d425359.mailgun.org";
  public static final String SEND_EMAIL = DOMAIN_URL + "/messages";
}

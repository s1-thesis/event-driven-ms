package com.ehy.message.application.dao.impl;

import com.ehy.message.application.dao.interfaces.EmailLogRepository;
import com.ehy.message.application.entity.domain.EmailLog;
import javax.enterprise.context.ApplicationScoped;

@ApplicationScoped
public class EmailLogRepositoryImpl extends BaseRepositoryImpl<EmailLog> implements
    EmailLogRepository {

}

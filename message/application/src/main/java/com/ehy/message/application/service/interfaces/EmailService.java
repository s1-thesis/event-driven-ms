package com.ehy.message.application.service.interfaces;

import com.ehy.common.model.MandatoryRequest;
import com.ehy.message.application.entity.domain.EmailLog;
import com.ehy.message.dto.EmailDto;
import io.smallrye.mutiny.Uni;

public interface EmailService extends BaseResourceService<EmailLog> {

  Uni<String> sendEmail(MandatoryRequest mandatoryRequest, EmailDto emailDto);
}

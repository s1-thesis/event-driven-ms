package com.ehy.message.application.web.interceptor;

import java.io.IOException;
import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.container.ContainerRequestFilter;
import javax.ws.rs.ext.Provider;
import lombok.extern.slf4j.Slf4j;

@Provider
@Slf4j
public class LoggingInterceptor implements ContainerRequestFilter {

  @Override
  public void filter(ContainerRequestContext context) throws IOException {
    String httpMethod = context.getMethod();
    String path = context.getUriInfo().getPath();
    String username = context.getHeaderString("username");
    log.info("Request method {}, path {}, by {}", httpMethod, path, username);
  }
}

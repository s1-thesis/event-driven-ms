package com.ehy.message.application.entity.constant;

public class ApiPath {

  /**
   * Rename this with service name
   */
  public static final String BASE_PATH = "/message-service";

  public static final String APPEND_SYSTEM_PARAMETERS = "/system-parameters";
  public static final String APPEND_ID = "/{id}";
  public static final String APPEND_EMAIL = "/email";
  public static final String APPEND_SEND = "/send";

  /**
   * Base path
   */
  public static final String BASE_PATH_SYSTEM_PARAMETERS = BASE_PATH + APPEND_SYSTEM_PARAMETERS;
  public static final String BASE_PATH_EMAIL = BASE_PATH + APPEND_EMAIL;
}

package com.ehy.message.application.dao.impl;

import com.ehy.message.application.dao.interfaces.SystemParameterRepository;
import com.ehy.message.application.entity.domain.SystemParameter;
import javax.enterprise.context.ApplicationScoped;

@ApplicationScoped
public class SystemParameterRepositoryImpl extends BaseRepositoryImpl<SystemParameter> implements SystemParameterRepository {

}

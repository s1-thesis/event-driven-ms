package com.ehy.message.application.libraries.mapper;

import com.ehy.message.application.entity.domain.SystemParameter;
import com.ehy.message.dto.SystemParameterDto;
import java.util.List;
import org.mapstruct.IterableMapping;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Named;

@Mapper(componentModel = "cdi")
public interface SystemParameterMapper extends BaseMapper {

  @Named("toDto")
  @Mapping(target = "id", source = "systemParameter.id", qualifiedByName = "objectIdToString")
  SystemParameterDto toDto(SystemParameter systemParameter);

  @Mapping(target = "id", source = "systemParameter.id", qualifiedByName = "stringToObjectId")
  SystemParameter toDomain(SystemParameterDto systemParameter);

  @IterableMapping(qualifiedByName = "toDto")
  List<SystemParameterDto> toDtoList(List<SystemParameter> systemParameters);
}

package com.ehy.message.dto.deserializer;

import com.ehy.message.dto.EmailDto;
import io.quarkus.kafka.client.serialization.ObjectMapperDeserializer;

public class EmailDtoDeserializer extends ObjectMapperDeserializer<EmailDto> {

  public EmailDtoDeserializer() {
    super(EmailDto.class);
  }
}

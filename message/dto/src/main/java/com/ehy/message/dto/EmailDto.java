package com.ehy.message.dto;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class EmailDto {

  @NotBlank @Email
  private String from;
  @NotBlank @Email
  private String to;
  @NotBlank
  private String subject;
  @NotBlank
  private String text;
  private String username;
}
